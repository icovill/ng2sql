﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
/*================================================================================================================
Script: dbo.AddConfigValues.sql 

Synopsis: 

Notes: 

==================================================================================================================
Revision History: 

Date			Author				Description
------------------------------------------------------------------------------------------------------------------
09/08/2017		Ian Covill			Script Created

==================================================================================================================*/
INSERT [dbo].[Config] (
	 [ConfigId]
	,[Value]
	,[Active]
	)
	VALUES 
	 (N'Inspection.InactiveElapsedHours', N'168', 1)
	,(N'InspectionName.All.Label',N'Inspection Name', 1)
	,(N'ProductCode.All.Label',N'Product Code', 1)