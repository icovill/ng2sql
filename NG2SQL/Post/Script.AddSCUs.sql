﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
/* Add the appropriate number of SCUs that will be connected to the SQL Server
   Fill in the values of each SCU, *MUST BE UNIQUE**/

:setvar SCU1Name		"SCU1"
:setvar SCU1Address		"//10.36.78.1"
:setvar SCU1Directory	"E:\XMLData\SCU1"
:setvar SCU1Active		"1"

--:setvar SCU2Name		"SCU2"
--:setvar SCU2Address		"//10.36.78.2"
--:setvar SCU2Directory	"E:\XMLData\SCU2"
--:setvar SCU2Active		"1"

--:setvar SCU3Name		"SCU3"
--:setvar SCU3Address		"//10.36.78.3"
--:setvar SCU3Directory	"E:\XMLData\SCU3"
--:setvar SCU3Active		"1"


INSERT [dbo].[SCUs] (
     [SCUName] 
    ,[NetworkAddress] 
    ,[InspectionFolder] 
    ,[Active]
	)
	VALUES
		(N'$SCU1Name', N'$SCU1Address', N'$SCU1Directory', $SCU1Active)
		--(N'$SCU2Name', N'$SCU2Address', N'$SCU2Directory', $SCU2Active)
		--(N'$SCU3Name', N'$SCU3Address', N'$SCU3Directory', $SCU3Active)

GOc