﻿CREATE FUNCTION [Utility].[DirectoryList]
(@Path NVARCHAR (255), @Filter NVARCHAR (5))
RETURNS 
     TABLE (
        [FullPath]         NVARCHAR (512) NULL,
        [Name]             NVARCHAR (512) NULL,
        [IsDirectory]      BIT            NULL,
        [Size]             BIGINT         NULL,
        [DateTimeCreated]  DATETIME       NULL,
        [DateTimeModified] DATETIME       NULL,
        [Extension]        NVARCHAR (10)  NULL)
AS
 EXTERNAL NAME [FileSystemHelper].[UserDefinedFunctions].[DirectoryList]









