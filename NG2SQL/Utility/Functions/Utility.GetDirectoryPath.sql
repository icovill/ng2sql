﻿CREATE FUNCTION [Utility].[GetDirectoryPath](@FullPath NVARCHAR(255)) 
RETURNS NVARCHAR(255) 
AS 
BEGIN 
    RETURN LEFT(@FullPath, LEN(@FullPath) - (CHARINDEX(N'\', REVERSE(@FullPath)) - 1)); 
END; 

