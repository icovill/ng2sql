﻿USE [master];
GO

EXECUTE [sys].[sp_configure] N'show advanced options', N'1'; 
GO 

RECONFIGURE WITH OVERRIDE; 
GO 

EXECUTE [sys].[sp_configure] N'min server memory (MB)', N'1024'; 
GO 

EXECUTE [sys].[sp_configure] N'max server memory (MB)', N'1024'; 
GO 

EXECUTE [sys].[sp_configure] N'fill factor (%)', N'95'; 
GO 

EXECUTE [sys].[sp_configure] N'optimize for ad hoc workloads', N'1'; 
GO 

EXECUTE [sys].[sp_configure] N'clr enabled', N'1'; 
GO

RECONFIGURE WITH OVERRIDE; 
GO 

EXECUTE [sys].[sp_configure] N'show advanced options', N'0'; 
GO 

RECONFIGURE WITH OVERRIDE; 
GO 
/*
ALTER DATABASE [model] SET RECOVERY SIMPLE WITH NO_WAIT;
GO

ALTER DATABASE [tempdb] MODIFY FILE ( NAME = N'templog', SIZE = 65536KB , FILEGROWTH = 65536KB );
GO

ALTER DATABASE [tempdb] MODIFY FILE ( NAME = N'tempdev', SIZE = 65536KB , FILEGROWTH = 65536KB );
GO

IF NOT EXISTS(SELECT name FROM [tempdb].[sys].[database_files] WHERE [name] = N'tempdev2')
BEGIN
	ALTER DATABASE [tempdb] ADD FILE ( NAME = N'tempdev2', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\tempdb2.ndf' , SIZE = 65536KB , FILEGROWTH = 65536KB ); 
END; 
GO

IF NOT EXISTS(SELECT name FROM [tempdb].[sys].[database_files] WHERE [name] = N'tempdev3')
BEGIN
	ALTER DATABASE [tempdb] ADD FILE ( NAME = N'tempdev3', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\tempdb3.ndf' , SIZE = 65536KB , FILEGROWTH = 65536KB );
END; 
GO

IF NOT EXISTS(SELECT name FROM [tempdb].[sys].[database_files] WHERE [name] = N'tempdev4')
BEGIN
	ALTER DATABASE [tempdb] ADD FILE ( NAME = N'tempdev4', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\tempdb4.ndf' , SIZE = 65536KB , FILEGROWTH = 65536KB );
END; 
GO
*/