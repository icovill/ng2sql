﻿/*
Deployment script for installing FileSystemHelper.dll assembly and security configuration

*/
:setvar FileSystemHelperPath "'E:\SQLData\MSSQL12.MSSQLSERVER\MSSQL\DATA\FileSystemHelper.dll'"

GO
:on error exit
GO
/*
Detect SQLCMD mode and disable script execution if SQLCMD mode is not supported.
To re-enable the script after enabling SQLCMD mode, execute the following:
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'SQLCMD mode must be enabled to successfully execute this script.';
        SET NOEXEC ON;
    END

GO
USE [master];

GO

IF EXISTS (SELECT * FROM [sys].[server_principals] WHERE [name] = N'FileSystemLogin') 
BEGIN 
    DROP LOGIN [FileSystemLogin]; 
END; 
GO 

IF EXISTS (SELECT * FROM [sys].[asymmetric_keys] WHERE name = N'FileSystemKey') 
BEGIN 
    DROP ASYMMETRIC KEY [FileSystemKey]; 
END; 
GO 

IF EXISTS (SELECT * FROM [sys].[assemblies] WHERE [name] = N'FileSystemHelper') 
BEGIN 
    DROP ASSEMBLY [FileSystemHelper]; 
END; 
GO 

IF EXISTS (SELECT * FROM [sys].[database_principals] WHERE [name] = N'FileSystemUser') 
BEGIN 
    DROP USER [FileSystemUser]; 
END; 
GO 

USE [master]; 
GO 

CREATE ASYMMETRIC KEY [FileSystemKey] FROM EXECUTABLE FILE = $(FileSystemHelperPath); 
GO

CREATE LOGIN [FileSystemLogin] FROM ASYMMETRIC KEY [FileSystemKey]; 
GO

GRANT EXTERNAL ACCESS ASSEMBLY TO [FileSystemLogin]; 
GO

USE CSQLDataStore; 
GO

CREATE USER [FileSystemUser] FOR LOGIN [FileSystemLogin]
GO

CREATE ASSEMBLY [FileSystemHelper] FROM $(FileSystemHelperPath) WITH PERMISSION_SET = EXTERNAL_ACCESS; 
GO
