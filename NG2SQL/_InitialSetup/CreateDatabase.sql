﻿/*
Deployment script for creating CSQLDataStore database

*/
GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON
GO
SET NUMERIC_ROUNDABORT OFF;
GO


/*
Fill in values that match the desired configuration of the SQL Server
*/

:setvar DatabaseName "NxtGen2DataStore"
:setvar DefaultFilePrefix "NxtGen2DataStore"
:setvar DefaultDataPath "E:\SQLData\MSSQL12.MSSQLSERVER\MSSQL\DATA"
:setvar DefaultLogPath "E:\SQLData\MSSQL12.MSSQLSERVER\MSSQL\DATA"

GO
:on error exit
GO
/*
Detect SQLCMD mode and disable script execution if SQLCMD mode is not supported.
To re-enable the script after enabling SQLCMD mode, execute the following:
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'SQLCMD mode must be enabled to successfully execute this script.';
        SET NOEXEC ON;
    END

GO
USE [master];

GO

/****** Object:  Database [CSQLDataStore]    Script Date: 10/11/2016 12:27:03 AM ******/
CREATE DATABASE [$(DatabaseName)]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'$(DatabaseName)', FILENAME = N'$(DefaultDataPath)$(DefaultFilePrefix).mdf' , SIZE = 65536KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB ), 
 FILEGROUP [DATA]  DEFAULT
( NAME = N'$(DatabaseName)_data', FILENAME = N'$(DefaultDataPath)$(DefaultFilePrefix)_data.ndf' , SIZE = 983040KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB ), 
 FILEGROUP [INDEX] 
( NAME = N'$(DatabaseName)_index', FILENAME = N'$(DefaultDataPath)$(DefaultFilePrefix)_index_mdf' , SIZE = 131072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'$(DatabaseName)_log', FILENAME = N'$(DefaultDataPath)$(DefaultFilePrefix)_log.ldf' , SIZE = 655360KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO

ALTER DATABASE [$(DatabaseName)] SET COMPATIBILITY_LEVEL = 120
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [$(DatabaseName)].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO

ALTER DATABASE [$(DatabaseName)] SET ANSI_NULL_DEFAULT OFF 
GO

ALTER DATABASE [$(DatabaseName)] SET ANSI_NULLS OFF 
GO

ALTER DATABASE [$(DatabaseName)] SET ANSI_PADDING OFF 
GO

ALTER DATABASE [$(DatabaseName)] SET ANSI_WARNINGS OFF 
GO

ALTER DATABASE [$(DatabaseName)] SET ARITHABORT OFF 
GO

ALTER DATABASE [$(DatabaseName)] SET AUTO_CLOSE OFF 
GO

ALTER DATABASE [$(DatabaseName)] SET AUTO_SHRINK OFF 
GO

ALTER DATABASE [$(DatabaseName)] SET AUTO_UPDATE_STATISTICS ON 
GO

ALTER DATABASE [$(DatabaseName)] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO

ALTER DATABASE [$(DatabaseName)] SET CURSOR_DEFAULT  GLOBAL 
GO

ALTER DATABASE [$(DatabaseName)] SET CONCAT_NULL_YIELDS_NULL OFF 
GO

ALTER DATABASE [$(DatabaseName)] SET NUMERIC_ROUNDABORT OFF 
GO

ALTER DATABASE [$(DatabaseName)] SET QUOTED_IDENTIFIER OFF 
GO

ALTER DATABASE [$(DatabaseName)] SET RECURSIVE_TRIGGERS OFF 
GO

ALTER DATABASE [$(DatabaseName)] SET  DISABLE_BROKER 
GO

ALTER DATABASE [$(DatabaseName)] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO

ALTER DATABASE [$(DatabaseName)] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO

ALTER DATABASE [$(DatabaseName)] SET TRUSTWORTHY OFF 
GO

ALTER DATABASE [$(DatabaseName)] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO

ALTER DATABASE [$(DatabaseName)] SET PARAMETERIZATION SIMPLE 
GO

ALTER DATABASE [$(DatabaseName)] SET READ_COMMITTED_SNAPSHOT OFF 
GO

ALTER DATABASE [$(DatabaseName)] SET HONOR_BROKER_PRIORITY OFF 
GO

ALTER DATABASE [$(DatabaseName)] SET RECOVERY SIMPLE 
GO

ALTER DATABASE [$(DatabaseName)] SET  MULTI_USER 
GO

ALTER DATABASE [$(DatabaseName)] SET PAGE_VERIFY CHECKSUM  
GO

ALTER DATABASE [$(DatabaseName)] SET DB_CHAINING OFF 
GO

ALTER DATABASE [$(DatabaseName)] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO

ALTER DATABASE [$(DatabaseName)] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO

ALTER DATABASE [$(DatabaseName)] SET DELAYED_DURABILITY = DISABLED 
GO

ALTER DATABASE [$(DatabaseName)] SET  READ_WRITE 
GO


