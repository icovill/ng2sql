﻿USE [master];
GO

PRINT N'Executing Security';
GO

--IF EXISTS (SELECT * FROM [sys].[server_principals] WHERE [name] = N'NT SERVICE\Winmgmt')
--BEGIN 
--	DROP LOGIN [NT SERVICE\Winmgmt];
--END; 
--GO 

--IF EXISTS (SELECT * FROM [sys].[server_principals] WHERE [name] = N'NT SERVICE\SQLWriter')
--BEGIN 
--	DROP LOGIN [NT SERVICE\SQLWriter];
--END; 
--GO 

--IF EXISTS (SELECT * FROM [sys].[server_principals] WHERE [name] = N'NT SERVICE\MSSQLSERVER')
--BEGIN 
--	DROP LOGIN [NT SERVICE\MSSQLSERVER];
--END; 
--GO 

IF EXISTS (SELECT * FROM [sys].[server_principals] WHERE [name] = N'BUILTIN\Users')
BEGIN 
	DROP LOGIN [BUILTIN\Users];
END; 
GO 

IF NOT EXISTS (SELECT * FROM [sys].[server_principals] WHERE [name] = N'dbowner')
BEGIN 
	CREATE LOGIN [dbowner] WITH PASSWORD=N'D@t@base!', DEFAULT_DATABASE=[master], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF; 
END; 
GO 

DENY CONNECT SQL TO [dbowner];
GO

-- This has been removed until it is decided if it will be used or not.

--IF NOT EXISTS (SELECT * FROM [sys].[server_principals] WHERE [name] = N'dbadmin')
--BEGIN 
--	CREATE LOGIN [dbadmin] WITH PASSWORD=N'D@t@base!', DEFAULT_DATABASE=[master], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF; 
--END; 
--GO 

IF EXISTS (SELECT * FROM [sys].[server_principals] WHERE [name] = N'dbadmin')
BEGIN 
	DROP LOGIN [dbadmin]; 
END; 
GO 

-- This has been removed until it is decided if it will be used or not.

--IF NOT EXISTS (SELECT * FROM [sys].[server_principals] WHERE [name] = CAST(SERVERPROPERTY('ServerName') AS NVARCHAR(128)) + N'\SQLServerUsers')
--BEGIN 
--	DECLARE @SQL AS NVARCHAR(4000);

--	SET @SQL = N'CREATE LOGIN [' + CAST(SERVERPROPERTY('ServerName') AS NVARCHAR(128)) + N'\SQLServerUsers] FROM WINDOWS WITH DEFAULT_DATABASE=[master];';

--	EXECUTE (@SQL);
--END;
--GO


IF EXISTS (SELECT * FROM [sys].[server_principals] WHERE [name] = CAST(SERVERPROPERTY('ServerName') AS NVARCHAR(128)) + N'\SQLServerUsers')
BEGIN 
	DECLARE @SQL AS NVARCHAR(4000);

	SET @SQL = N'DROP LOGIN [' + CAST(SERVERPROPERTY('ServerName') AS NVARCHAR(128)) + N'\SQLServerUsers];';

	EXECUTE (@SQL);
END; 
GO 

USE [ReportData];
GO
IF NOT EXISTS (SELECT * FROM [sys].[server_principals] WHERE [name] = N'NT AUTHORITY\LOCAL SERVICE')
CREATE LOGIN [NT AUTHORITY\LOCAL SERVICE] FROM WINDOWS WITH DEFAULT_DATABASE=[master], DEFAULT_LANGUAGE=[us_english]
GO

ALTER SERVER ROLE [sysadmin] ADD MEMBER [NT AUTHORITY\LOCAL SERVICE]
GO