﻿CREATE VIEW [dbo].[InspectionList]
	AS 
	SELECT	 C.[Id]
			,DL.[FullPath]
			,DL.[IsDirectory]
	FROM	[dbo].[SCUs] AS C CROSS APPLY
			[Utility].[DirectoryList]([dbo].[DataDirectory](C.Id,DEFAULT),NULL) as DL
