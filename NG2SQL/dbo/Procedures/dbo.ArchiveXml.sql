﻿
/*================================================================================================================
Script: dbo.ArchiveFile.sql

Synopsis: 

Notes:

==================================================================================================================
Revision History:

Date			Author				Description
------------------------------------------------------------------------------------------------------------------
06/21/2016		Bob Stixrud			Script Created
09/07/2017		Ian Covill			Corrections to function with NxtGen2

==================================================================================================================*/
CREATE PROCEDURE [dbo].[ArchiveXml]
(
	@Debug AS BIT = 0, 
	@SCUID INT,
	@InspectionID AS INT, 
	@FullPath AS NVARCHAR(255), 
	@IsDirectory AS BIT
)
AS
BEGIN
	---------------------------------------------------------------------------------------------------
	--// SET STATEMENTS                                                                            //--
	---------------------------------------------------------------------------------------------------
	SET NOCOUNT ON;
	SET QUOTED_IDENTIFIER ON;
	SET ARITHABORT OFF;
	SET NUMERIC_ROUNDABORT OFF;
	SET ANSI_WARNINGS ON;
	SET ANSI_PADDING ON;
	SET ANSI_NULLS ON;
	SET CONCAT_NULL_YIELDS_NULL ON;
	SET CURSOR_CLOSE_ON_COMMIT OFF;
	SET IMPLICIT_TRANSACTIONS OFF;
	SET DATEFORMAT MDY;
	SET DATEFIRST 7;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	---------------------------------------------------------------------------------------------------
	--// DECLARATIONS                                                                              //--
	---------------------------------------------------------------------------------------------------
	DECLARE @ArchiveFile AS BIT;
	DECLARE @FourDigitYear AS NVARCHAR(4);
	DECLARE @TwoDigitMonth AS NVARCHAR(2);
	DECLARE @InspectionStartTime AS DATETIME;
	DECLARE @Destination AS NVARCHAR(255);
	DECLARE @Source AS NVARCHAR(255);

	IF (@Debug = 1) EXECUTE [Debug].[Write] @Indent=2, @Text=N'Executing: [dbo].[ArchiveFile]...'; 

	SET @ArchiveFile = 0; 
	SET @Source = @FullPath;

	IF 
	(
		EXISTS (SELECT * FROM [dbo].[Inspections] WHERE [InspectionEndTime] IS NOT NULL AND [Id] = @InspectionID)
		AND NOT EXISTS (SELECT * FROM [dbo].[AllPendingInspections](@SCUID, @InspectionID, @FullPath, @IsDirectory))
	)
	BEGIN
		IF (@Debug = 1) EXECUTE [Debug].[Write] @Indent=3, @Text=N'Archive conditions have been met...'; 

		SET @InspectionStartTime = (SELECT [InspectionStartTime] FROM [dbo].[Inspections] WHERE [Id] = @InspectionID); 

		IF (@InspectionStartTime IS NOT NULL)
		BEGIN
			
			SET @FourDigitYear = CAST(DATEPART(YEAR, @InspectionStartTime) AS NVARCHAR(4)) ;
			SET @TwoDigitMonth = RIGHT('00'+ CAST(DATEPART(MONTH, @InspectionStartTime) AS NVARCHAR(2)), 2);

			SET @Destination = [dbo].[ArchiveDirectory](@SCUID,'\') + @FourDigitYear + N'\' + @TwoDigitMonth + N'\';

			EXECUTE [Utility].[DirectoryCreate] @Destination;

			SET @Destination = @Destination + REPLACE(@FullPath, [dbo].[DataDirectory](@SCUID,'\'), N'');
		
			IF (@IsDirectory = 1) EXECUTE [Utility].[DirectoryMove] @Source, @Destination; 
		
			IF (@IsDirectory = 0) EXECUTE [Utility].[FileMove] @Source, @Destination;

			UPDATE	[dbo].[Inspections]
				SET		[ArchivePath] = N'\' + @FourDigitYear + N'\' + @TwoDigitMonth 
				WHERE [Id] = @InspectionID
			
			DELETE	[dbo].[PendingInspections]
			WHERE	[InspectionID] = @InspectionID; 

		END;
	END; 

	IF (@Debug = 1) EXECUTE [Debug].[Write] @Indent=2, @Text=N'Completed!'; 
END; 

