﻿
/*================================================================================================================
Script: dbo.CreateRollID.sql

Synopsis: 

Notes:

==================================================================================================================
Revision History:

Date			Author				Description
------------------------------------------------------------------------------------------------------------------
06/21/2016		Bob Stixrud			Script Created

==================================================================================================================*/
CREATE PROCEDURE [dbo].[CreateInspectionID] 
( 
	@Debug AS BIT = 0, 
	@SCUID INT,
	@FullPath AS NVARCHAR(255),
	@InspectionString AS NVARCHAR(255),
	@InspectionStartTime DATETIME,
	@Recipe AS NVARCHAR(255),
	@ProductCode AS NVARCHAR(255), 
	@InspectionID AS INT OUTPUT 
) 
AS 
BEGIN 
	---------------------------------------------------------------------------------------------------
	--// SET STATEMENTS                                                                            //--
	---------------------------------------------------------------------------------------------------
	SET NOCOUNT ON; 
	SET QUOTED_IDENTIFIER ON; 
	SET ARITHABORT OFF; 
	SET NUMERIC_ROUNDABORT OFF; 
	SET ANSI_WARNINGS ON; 
	SET ANSI_PADDING ON; 
	SET ANSI_NULLS ON; 
	SET CONCAT_NULL_YIELDS_NULL ON; 
	SET CURSOR_CLOSE_ON_COMMIT OFF; 
	SET IMPLICIT_TRANSACTIONS OFF; 
	SET DATEFORMAT MDY; 
	SET DATEFIRST 7; 
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; 

	DECLARE @IdentityOutput AS TABLE ([ID] BIGINT); 

	IF (@Debug = 1) EXECUTE [Debug].[Write] @Indent=2, @Text=N'Executing: [dbo].[CreateInspectionID]...'; 
	IF (@Debug = 1) EXECUTE [Debug].[Write] @Indent=3, @Text=N''; 

	INSERT INTO [dbo].[Inspections] ([SCUID], [InspectionName], [InspectionStartTime], [Recipe], [ProductCode]) 
	OUTPUT INSERTED.[Id] INTO @IdentityOutput 
	VALUES (@SCUID, ISNULL(@InspectionString,N''), @InspectionStartTime, @Recipe, ISNULL(@ProductCode,@Recipe)); 

	SET @InspectionID = (SELECT TOP(1) [ID] FROM @IdentityOutput); 

	INSERT [dbo].[PendingInspections] ([InspectionID], [InspectionPath]) VALUES (@InspectionID, @FullPath)

	IF (@Debug = 1) EXECUTE [Debug].[Write] @Indent=2, @Text=N'Completed!'; 
END; 

