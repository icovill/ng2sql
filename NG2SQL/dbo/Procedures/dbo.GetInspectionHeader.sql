﻿/*================================================================================================================
Script: dbo.GetInspectionHeader.sql

Synopsis: 

Notes:

==================================================================================================================
Revision History:

Date			Author				Description
------------------------------------------------------------------------------------------------------------------
09/06/2017		Ian Covill			Script Created

==================================================================================================================*/
CREATE PROCEDURE [dbo].[GetInspectionHeader] 
( 
	@Debug AS BIT = 0, 
	@SCUID INT,
	@IsDirectory AS BIT, 
	@FullPath AS NVARCHAR(255) OUTPUT, 
	@InspectionName AS NVARCHAR(255) OUTPUT,
	@InspectionStartTime DATETIME OUTPUT,
	@Recipe AS NVARCHAR(255) OUTPUT,
	@ProductCode AS NVARCHAR(255) OUTPUT 
) 
AS 
BEGIN 
	---------------------------------------------------------------------------------------------------
	--// SET STATEMENTS                                                                            //--
	---------------------------------------------------------------------------------------------------
	SET NOCOUNT ON; 
	SET QUOTED_IDENTIFIER ON; 
	SET ARITHABORT OFF; 
	SET NUMERIC_ROUNDABORT OFF; 
	SET ANSI_WARNINGS ON; 
	SET ANSI_PADDING ON; 
	SET ANSI_NULLS ON; 
	SET CONCAT_NULL_YIELDS_NULL ON; 
	SET CURSOR_CLOSE_ON_COMMIT OFF; 
	SET IMPLICIT_TRANSACTIONS OFF; 
	SET DATEFORMAT MDY; 
	SET DATEFIRST 7; 
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; 

	---------------------------------------------------------------------------------------------------
	--// DECLARATIONS                                                                              //--
	---------------------------------------------------------------------------------------------------
	DECLARE @XmlHandle      AS INT; 
	DECLARE @XmlText        AS XML; 
	DECLARE @XmlPath        AS NVARCHAR(64); 
	DECLARE @SQLString      AS NVARCHAR(1000); 
	DECLARE @ParmDefinition AS NVARCHAR(200); 

	IF (@Debug = 1) EXECUTE [Debug].[Write] @Indent=2, @Text=N'Executing: [dbo].[GetInspectionHeader]...'; 
	IF (@Debug = 1) EXECUTE [Debug].[Write] @Indent=3, @Text=N'Input-Parameter: @FullPath = ', @NVARCHAR=@FullPath; 
	IF (@Debug = 1) EXECUTE [Debug].[Write] @Indent=3, @Text=N'Input-Parameter: @IsDirectory = ', @BIT=@IsDirectory; 

	---------------------------------------------------------------------------------------------------
	--// IMPORT XML USING OPENROWSET                                                               //--
	---------------------------------------------------------------------------------------------------
	BEGIN TRY 

		IF (@Debug = 1) EXECUTE [Debug].[Write] @Indent=3, @Text=N'Entering Cursor [C]:'; 

		IF ((SELECT CURSOR_STATUS('global', 'C')) >= -1) 
		BEGIN 
			IF ((SELECT CURSOR_STATUS('global', 'C')) > -1) 
			BEGIN 
				CLOSE [C]; 
			END; 
			DEALLOCATE [C]; 
		END; 

		DECLARE  [C] CURSOR FAST_FORWARD FOR 
		SELECT	 [FullPath], [XmlPath] 
		FROM	 [dbo].[PossibleLocations](@FullPath, @IsDirectory) 

		OPEN [C]; 
		FETCH NEXT FROM [C] INTO @FullPath, @XmlPath; 
		WHILE (@@FETCH_STATUS = 0) 
		BEGIN 

			IF (@Debug = 1) EXECUTE [Debug].[Write] @Indent=4, @Text=N'Parameter: @FullPath = ', @NVARCHAR=@FullPath; 
			IF (@Debug = 1) EXECUTE [Debug].[Write] @Indent=4, @Text=N'Parameter: @XmlPath  = ', @NVARCHAR=@XmlPath; 

			SET @SQLString = N'SELECT @XmlText = CONVERT(XML, [BulkColumn]) FROM OPENROWSET(BULK ''' + @FullPath + ''', SINGLE_BLOB) AS [Source];'; 

			SET @ParmDefinition = N'@XmlText XML OUTPUT'; 

			EXECUTE [dbo].[sp_executesql] @SQLString, @ParmDefinition, @XmlText OUTPUT; 

			EXECUTE [dbo].[sp_xml_preparedocument] @XmlHandle OUTPUT, @XmlText; 

			IF (@Debug = 1) EXECUTE [Debug].[Write] @Indent=4, @Text=N'Assigning value from XML element InspectionStartTime/'; 

			SELECT	 @InspectionName = ISNULL([xml].[InspectionString], N'')
					,@InspectionStartTime = ISNULL([xml].[InspectionStartTime], CONVERT(DATETIME, 0))
					,@Recipe = ISNULL([xml].[Recipe], N'')
					,@ProductCode = ISNULL([xml].[ProductCode], @Recipe)
			FROM	OPENXML(@XmlHandle, @XmlPath, 1) 
					WITH (
						 [InspectionString]		NVARCHAR(255)	'InspectionID'
						,[InspectionStartTime]	DATETIME		'InspectionStartTime'
						,[Recipe]				NVARCHAR(255)	'Recipe'
						,[ProductCode]			NVARCHAR(255)	'ProductCode'
						) AS [xml]; 

			EXECUTE [dbo].[sp_xml_removedocument] @XmlHandle; 

			IF (@InspectionStartTime IS NOT NULL) BREAK;

			FETCH NEXT FROM [C] INTO @FullPath, @XmlPath; 
		END; 
		CLOSE [C]; 
		DEALLOCATE [C]; 

	END TRY 
	BEGIN CATCH 

		DECLARE @ErrorNumber    AS INT; 
		DECLARE @ErrorSeverity  AS INT; 
		DECLARE @ErrorState     AS INT; 
		DECLARE @ErrorLine      AS INT; 
		DECLARE @ErrorProcedure AS NVARCHAR(200); 
		DECLARE @ErrorMessage   AS NVARCHAR(4000); 

		SET @ErrorNumber    = ERROR_NUMBER(); 
		SET @ErrorSeverity  = ERROR_SEVERITY(); 
		SET @ErrorState     = ERROR_STATE(); 
		SET @ErrorLine      = ERROR_LINE(); 
		SET @ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-'); 
		SET @ErrorMessage   = [dbo].[GetErrorString](@ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorLine, @ErrorProcedure, ERROR_MESSAGE()); 

		IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION; 

		EXECUTE [dbo].[ProcessError] @SCUID, 0, @FullPath, @ErrorNumber, @ErrorMessage; 

		RAISERROR ('*** ERROR *** Check the error table', 0, 1, @FullPath) WITH NOWAIT; 
		RAISERROR (@ErrorMessage, @ErrorSeverity, 1); 

	END CATCH 

	IF (@Debug = 1) EXECUTE [Debug].[Write] @Indent=5, @Text=N'Parameter: @InspectionStartTime = ', @DATETIME=@InspectionStartTime; 
	IF (@Debug = 1) EXECUTE [Debug].[Write] @Indent=2, @Text=N'Completed!'; 
END;