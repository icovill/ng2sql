﻿/*================================================================================================================
Script: dbo.DeactivateOrphanedInspections.sql

Synopsis: 

Notes:

==================================================================================================================
Revision History:

Date			Author				Description
------------------------------------------------------------------------------------------------------------------
09/07/2017		Ian Covill			Script Created

==================================================================================================================*/
CREATE PROCEDURE [dbo].[DeactivateOrphanedInspections]
( 
	@Debug AS BIT = 0, 
	@InspectionElapsedHours INT
) 
AS 
BEGIN 
	---------------------------------------------------------------------------------------------------
	--// SET STATEMENTS                                                                            //--
	---------------------------------------------------------------------------------------------------
	SET NOCOUNT ON; 
	SET QUOTED_IDENTIFIER ON; 
	SET ARITHABORT OFF; 
	SET NUMERIC_ROUNDABORT OFF; 
	SET ANSI_WARNINGS ON; 
	SET ANSI_PADDING ON; 
	SET ANSI_NULLS ON; 
	SET CONCAT_NULL_YIELDS_NULL ON; 
	SET CURSOR_CLOSE_ON_COMMIT OFF; 
	SET IMPLICIT_TRANSACTIONS OFF; 
	SET DATEFORMAT MDY; 
	SET DATEFIRST 7; 
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; 

	-- deactivate all rolls where inspection start is beyond the typical elapsed period
	DELETE FROM [dbo].[PendingInspections]
	WHERE [InspectionID] IN 
		(SELECT [Id] FROM [dbo].[Inspections] WHERE InspectionStartTime <= DATEADD(HOUR,-ABS(@InspectionElapsedHours), GETDATE()))

END;