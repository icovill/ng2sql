﻿
/*================================================================================================================
Script: dbo.ShredXmlDefect.sql

Synopsis: 

Notes:

==================================================================================================================
Revision History:

Date			Author				Description
------------------------------------------------------------------------------------------------------------------
09/7/2017		Ian Covill			Script Created

==================================================================================================================*/
CREATE PROCEDURE [dbo].[GetDefects]
( 
	@Debug AS BIT = 0, 
	@InspectionID AS INT, 
	@SourceFile AS NVARCHAR(255), 
	@XmlPath AS NVARCHAR(64) 
) 
AS 
BEGIN 
	---------------------------------------------------------------------------------------------------
	--// SET STATEMENTS                                                                            //--
	---------------------------------------------------------------------------------------------------
	SET NOCOUNT ON; 
	SET QUOTED_IDENTIFIER ON; 
	SET ARITHABORT OFF; 
	SET NUMERIC_ROUNDABORT OFF; 
	SET ANSI_WARNINGS ON; 
	SET ANSI_PADDING ON; 
	SET ANSI_NULLS ON; 
	SET CONCAT_NULL_YIELDS_NULL ON; 
	SET CURSOR_CLOSE_ON_COMMIT OFF; 
	SET IMPLICIT_TRANSACTIONS OFF; 
	SET DATEFORMAT MDY; 
	SET DATEFIRST 7; 
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; 

	---------------------------------------------------------------------------------------------------
	--// DECLARATIONS                                                                              //--
	---------------------------------------------------------------------------------------------------
	DECLARE @XmlHandle      AS INT; 
	DECLARE @XmlText        AS XML; 
	DECLARE @SQLString      AS NVARCHAR(1000); 
	DECLARE @ParmDefinition AS NVARCHAR(200); 
	DECLARE @DefectID       AS INT; 
	DECLARE @SourceFileID   AS INT; 
	DECLARE @IdentityOutput AS TABLE ([ID] INT); 

	IF (@Debug = 1) EXECUTE [Debug].[Write] @Indent=3, @Text=N'Executing: [dbo].[ShredXmlDefect]...'; 
	IF (@Debug = 1) EXECUTE [Debug].[Write] @Indent=4, @Text=N''; 

	---------------------------------------------------------------------------------------------------
	--// IMPORT XML USING OPENROWSET                                                               //--
	---------------------------------------------------------------------------------------------------
	SET @SQLString = N'SELECT @XmlText = CONVERT(XML, [BulkColumn]) FROM OPENROWSET(BULK ''' + @SourceFile + ''', SINGLE_BLOB) AS [Source];'; 

	SET @ParmDefinition = N'@XmlText XML OUTPUT'; 

	EXECUTE [dbo].[sp_executesql] @SQLString, @ParmDefinition, @XmlText OUTPUT; 

	---------------------------------------------------------------------------------------------------
	--// GET EXISTING OR CREATE NEW FILE                                                           //--
	---------------------------------------------------------------------------------------------------
	--EXECUTE [dbo].[GetSourceFileID] @Debug, @RollID, @SourceFile, @SourceFileID OUTPUT; 

	---------------------------------------------------------------------------------------------------
	--// INSERT USING OPENXML                                                                      //--
	---------------------------------------------------------------------------------------------------
	EXECUTE [dbo].[sp_xml_preparedocument] @XmlHandle OUTPUT, @XmlText; 

	IF (@XmlPath <> N'/Defect')
		SET @XmlPath = N'Inspection/Defects/Defect'; 

	INSERT INTO [dbo].[Defects] 
	( 
			 [DefectIndex]
			,[DefectSaveIndex] 
			,[CDPos]
			,[MDPos]
			,[Area]
			,[BBoxSizeX] 
			,[BBoxSizeY]
			,[TrueBreadth] 
			,[TrueLength] 
			,[TrueElongation]
			,[FeretMinDiameter] 
			,[FeretMeanDiameter] 
			,[FeretMaxDiameter] 
			,[FeretMaxAngle] 
			,[FeretElongation] 
			,[GrayMin]
			,[GrayMax] 
			,[GrayAvg]
			,[ClassName] 
			,[DefectType] 
			,[Action]
			,[ImageFileName]
			,[TimeStamp]
			,[InspectionID]
	) 
	OUTPUT	 INSERTED.[Id] INTO @IdentityOutput 
	SELECT	 [DefectIndex]			=	[xml].[DefectIndex]		
			,[DefectSaveIndex]		=	[xml].[DefectSaveIndex]	
			,[CDPos]				=	[xml].[CDPos]			
			,[MDPos]				=	[xml].[MDPos]			
			,[Area]					=	[xml].[Area]				
			,[BBoxSizeX]			=	[xml].[BBoxSizeX]		
			,[BBoxSizeY]			=	[xml].[BBoxSizeY]		 
			,[TrueBreadth]			=	[xml].[TrueBreadth]		 
			,[TrueLength]			=	[xml].[TrueLength]		
			,[TrueElongation]		=	[xml].[TrueElongation]	
			,[FeretMinDiameter]		=	[xml].[FeretMinDiameter]	
			,[FeretMeanDiameter]	=	[xml].[FeretMeanDiameter]
			,[FeretMaxDiameter]		=	[xml].[FeretMaxDiameter]	
			,[FeretMaxAngle]		=	[xml].[FeretMaxAngle]	
			,[FeretElongation]		=	[xml].[FeretElongation]	
			,[GrayMin]				=	[xml].[GrayMin]			
			,[GrayMax]				=	[xml].[GrayMax]			
			,[GrayAvg]				=	[xml].[GrayAvg]			
			,[ClassName]			=	ISNULL([xml].[ClassName], N'')		
			,[DefectType]			=	ISNULL([xml].[DefectType], N'')		
			,[Action]				=	ISNULL([xml].[Action], N'')			
			,[ImageFileName]		=	ISNULL([xml].[ImageFileName], N'') 
			,[TimeStamp]			=	ISNULL([xml].[TimeStamp], CONVERT(DATETIME, 0)) 
			,[InspectionID]			=	@InspectionID
		FROM OPENXML(@XmlHandle, @XmlPath, 1) 
			 WITH 
			 ( 
				 [DefectIndex]		INT				'DefectIndex' 
				,[DefectSaveIndex]  INT				'DefectSaveIndex' 
				,[CDPos]			FLOAT			'CDPos' 
				,[MDPos]			FLOAT			'MDPos' 
				,[Area]				FLOAT			'Area' 
				,[BBoxSizeX]		FLOAT			'BBoxSizeX' 
				,[BBoxSizeY]		FLOAT			'BBoxSizeY' 
				,[TrueBreadth]		FLOAT			'TrueBreadth' 
				,[TrueLength]		FLOAT			'TrueLength' 
				,[TrueElongation]	FLOAT			'TrueElongation' 
				,[FeretMinDiameter]	FLOAT			'FeretMinDiameter' 
				,[FeretMeanDiameter]FLOAT			'FeretMeanDiameter' 
				,[FeretMaxDiameter]	FLOAT			'FeretMaxDiameter' 
				,[FeretMaxAngle]	FLOAT			'FeretMaxAngle' 
				,[FeretElongation]	FLOAT			'FeretElongation' 
				,[GrayMin]			INT				'GrayMin' 
				,[GrayMax]			INT				'GrayMax' 
				,[GrayAvg]			FLOAT				'GrayAvg' 
				,[ClassName]		NVARCHAR(64)	'ClassName' 
				,[DefectType]		NVARCHAR(64)	'DefectType' 
				,[Action]			NVARCHAR(64)	'Action' 
			  	,[ImageFileName]	NVARCHAR(64)	'ImageFileName'
			    ,[TimeStamp]		DATETIME		'TimeStamp'
			)

		AS [xml];
	--SET @DefectID = (SELECT TOP(1) [ID] FROM @IdentityOutput); 

	--INSERT INTO [dbo].[DensityData]
	--(
	--		 [DefectID] 
	--		,[DefectDensity] 
	--		,[SameDefectDensity] 
	--		,[PixelDensity] 
	--		,[Action] 
	--)
	--SELECT	 [DefectID]          = @DefectID 
	--		,[DefectDensity]     = [xml].[DefectDensity] 
	--		,[SameDefectDensity] = [xml].[SameDefectDensity] 
	--		,[PixelDensity]      = [xml].[PixelDensity] 
	--		,[Action]            = [xml].[Action] 
	--FROM	 OPENXML(@XmlHandle, @XmlPath, 1) 
	--		 WITH 
	--		 ( 
	--			 [DefectDensity]     FLOAT 'DensityData/DefectDensity/@Value' 
	--			,[SameDefectDensity] FLOAT 'DensityData/SameDefectDensity/@Value' 
	--			,[PixelDensity]      FLOAT 'DensityData/PixelDensity/@Value' 
	--			,[Action]            FLOAT 'DensityData/Action/@Value' 
	--		 ) 
	--		 AS [xml]; 

	EXECUTE [dbo].[sp_xml_removedocument] @XmlHandle; 

	IF (@Debug = 1) EXECUTE [Debug].[Write] @Indent=3, @Text=N'Completed!'; 
END; 

