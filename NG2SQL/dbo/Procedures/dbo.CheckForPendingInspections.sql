﻿CREATE PROCEDURE [dbo].[CheckForPendingInspections]
(
	@Debug AS BIT = 0
)
AS
BEGIN
	---------------------------------------------------------------------------------------------------
	--// SET STATEMENTS                                                                            //--
	---------------------------------------------------------------------------------------------------
	SET NOCOUNT ON;
	SET QUOTED_IDENTIFIER ON;
	SET ARITHABORT OFF;
	SET NUMERIC_ROUNDABORT OFF;
	SET ANSI_WARNINGS ON;
	SET ANSI_PADDING ON;
	SET ANSI_NULLS ON;
	SET CONCAT_NULL_YIELDS_NULL ON;
	SET CURSOR_CLOSE_ON_COMMIT OFF;
	SET IMPLICIT_TRANSACTIONS OFF;
	SET DATEFORMAT MDY;
	SET DATEFIRST 7;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	---------------------------------------------------------------------------------------------------
	--// DECLARATIONS                                                                              //--
	---------------------------------------------------------------------------------------------------
	
	DECLARE @SCUID   AS INT; 
	DECLARE @InspectionID      AS INT; 
	DECLARE @IsDirectory AS BIT; 
	DECLARE @FullPath    AS NVARCHAR(255); 

	IF ((SELECT CURSOR_STATUS('global', 'ProcessingCursor')) >= -1) 
	BEGIN 
		IF ((SELECT CURSOR_STATUS('global', 'ProcessingCursor')) > -1) 
		BEGIN 
			CLOSE [ProcessingCursor]; 
		END; 
		DEALLOCATE [ProcessingCursor]; 
	END; 

	DECLARE  [ProcessingCursor] CURSOR FAST_FORWARD FOR
	SELECT	 C.[Id]
	        ,DL.[FullPath] 
			,DL.[IsDirectory] 
	FROM     [dbo].[SCUs] as C
	CROSS APPLY [Utility].[DirectoryList]([dbo].[DataDirectory](C.Id, DEFAULT),NULL) as DL
	WHERE Active = 1
	ORDER BY DL.[IsDirectory] DESC
			,DL.[Name] ASC;

	OPEN [ProcessingCursor]; 
	FETCH NEXT FROM [ProcessingCursor] INTO @SCUID, @FullPath, @IsDirectory; 
	WHILE (@@FETCH_STATUS = 0) 
	BEGIN 
		--SET @InspectionID = NULL; 
		IF (@Debug = 1) EXECUTE [Debug].[Write] @Text=N'*** Processing *** : ', @NVARCHAR=@FullPath; 

		EXECUTE [dbo].[GetInspectionID] @Debug, @SCUID, @FullPath, @IsDirectory, @InspectionID OUTPUT; 
	
		IF (@Debug = 1) EXECUTE [Debug].[Write] @Indent=1, @Text=N'Output-Parameter: @RollID = ', @INT=@InspectionID; 

		IF (@InspectionID IS NOT NULL)
		BEGIN 

			EXECUTE [dbo].[ProcessXML] @Debug, @SCUID, @InspectionID, @FullPath, @IsDirectory; 

			EXECUTE [dbo].[ArchiveXml] @Debug, @SCUID, @InspectionID, @FullPath, @IsDirectory; 
			
		END;

		FETCH NEXT FROM [ProcessingCursor] INTO @SCUID, @FullPath, @IsDirectory; 
	END; 
	CLOSE [ProcessingCursor]; 
	DEALLOCATE [ProcessingCursor]; 

	IF (@Debug = 1) EXECUTE [Debug].[Write] @Text=N'*** Completed *** : ', @NVARCHAR=@FullPath; 

	-- Deactivate potential ophaned inspections after elapsed hours
	DECLARE @InspectionElapsedHours INT = [dbo].[GetConfigValue] ('Inspection.InactiveAfterElapsedHours')
	
	EXEC [dbo].[DeactivateOrphanedInspections] @Debug, @InspectionElapsedHours  = @InspectionElapsedHours

END; 

