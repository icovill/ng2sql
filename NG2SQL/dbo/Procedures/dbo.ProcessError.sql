﻿
/*================================================================================================================
Script: dbo.ProcessError.sql

Synopsis: 

Notes:

==================================================================================================================
Revision History:

Date			Author				Description
------------------------------------------------------------------------------------------------------------------
06/21/2016		Bob Stixrud			Script Created

==================================================================================================================*/
CREATE PROCEDURE [dbo].[ProcessError]
(
	@SCUID INT,
	@InspectionID INT, 
	@SourceFileName AS NVARCHAR(255), 
	@ErrorNumber AS INT, 
	@ErrorMessage AS NVARCHAR(4000)  
)
AS
BEGIN

	---------------------------------------------------------------------------------------------------
	--// SET STATEMENTS                                                                            //--
	---------------------------------------------------------------------------------------------------
	SET NOCOUNT ON;
	SET QUOTED_IDENTIFIER ON;
	SET ARITHABORT OFF;
	SET NUMERIC_ROUNDABORT OFF;
	SET ANSI_WARNINGS ON;
	SET ANSI_PADDING ON;
	SET ANSI_NULLS ON;
	SET CONCAT_NULL_YIELDS_NULL ON;
	SET CURSOR_CLOSE_ON_COMMIT OFF;
	SET IMPLICIT_TRANSACTIONS OFF;
	SET DATEFORMAT MDY;
	SET DATEFIRST 7;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	---------------------------------------------------------------------------------------------------
	--// DECLARATIONS                                                                              //--
	---------------------------------------------------------------------------------------------------
	DECLARE @DestinationFileName AS NVARCHAR(255);
	DECLARE @SourceFileID AS INT;

	--EXECUTE [dbo].[GetSourceFileID] 1, @InspectionID, @SourceFileName, @SourceFileID OUTPUT;

	INSERT INTO [dbo].[Errors]
	(
			 [ErrorMessage]
			,[TimeStamp]
			,[InspectionID] 
	) 
	SELECT	 [ErrorMessage] = @ErrorMessage
			,[TimeStamp] = CURRENT_TIMESTAMP
			,[InspectionID] = @InspectionID; 

	-- Move entire inspection directory - quick fix 2010/26/2016 MM
	-- Get parent inspection directory based on current file  and move to error
	
	DECLARE @InspectionDir varchar(500) = LEFT(@SourceFileName, LEN(@SourceFileName) -  CHARINDEX('\',REVERSE(@SourceFileName), 1) + 1)

	DECLARE @InspectionErrorDir AS NVARCHAR(255);
	SET @InspectionErrorDir = REPLACE(@InspectionDir, [dbo].[DataDirectory](@SCUID, N'\'), [dbo].[ErrorDirectory](@SCUID, N'\'));

	EXEC [Utility].[DirectoryMove] @InspectionDir, @InspectionErrorDir

END;

