﻿
/*================================================================================================================
Script: dbo.ProcessXML.sql

Synopsis: 

Notes:

==================================================================================================================
Revision History:

Date			Author				Description
------------------------------------------------------------------------------------------------------------------
06/21/2016		Bob Stixrud			Script Created

==================================================================================================================*/
CREATE PROCEDURE [dbo].[ProcessXML]
( 
	@Debug AS BIT = 0, 
	@SCUID INT,
	@InspectionID AS INT, 
	@FullPath AS NVARCHAR(255), 
	@IsDirectory AS BIT 
) 
AS 
BEGIN 
	---------------------------------------------------------------------------------------------------
	--// SET STATEMENTS                                                                            //--
	---------------------------------------------------------------------------------------------------
	SET NOCOUNT ON;
	SET QUOTED_IDENTIFIER ON;
	SET ARITHABORT OFF;
	SET NUMERIC_ROUNDABORT OFF;
	SET ANSI_WARNINGS ON;
	SET ANSI_PADDING ON;
	SET ANSI_NULLS ON;
	SET CONCAT_NULL_YIELDS_NULL ON;
	SET CURSOR_CLOSE_ON_COMMIT OFF;
	SET IMPLICIT_TRANSACTIONS OFF;
	SET DATEFORMAT MDY;
	SET DATEFIRST 7;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	---------------------------------------------------------------------------------------------------
	--// DECLARATIONS                                                                              //--
	---------------------------------------------------------------------------------------------------
	DECLARE @SourceFile AS NVARCHAR(256); 
	DECLARE @XmlPath    AS NVARCHAR(64); 
	DECLARE @FileName   AS NVARCHAR(35); 

	IF (@Debug = 1) EXECUTE [Debug].[Write] @Indent=2, @Text=N'Executing: [dbo].[ProcessXML]...'; 
	IF (@Debug = 1) EXECUTE [Debug].[Write] @Indent=3, @Text=N'Input-Parameter: @InspectionID = ', @INT=@InspectionID; 
	IF (@Debug = 1) EXECUTE [Debug].[Write] @Indent=3, @Text=N'Input-Parameter: @FullPath = ', @NVARCHAR=@FullPath; 
	IF (@Debug = 1) EXECUTE [Debug].[Write] @Indent=3, @Text=N'Input-Parameter: @IsDirectory = ', @BIT=@IsDirectory; 

	IF ((SELECT CURSOR_STATUS('global', 'FileCursor')) >= -1) 
	BEGIN 
		IF ((SELECT CURSOR_STATUS('global', 'FileCursor')) > -1) 
		BEGIN 
			CLOSE [FileCursor]; 
		END; 
		DEALLOCATE [FileCursor]; 
	END; 

	DECLARE  [FileCursor] CURSOR FAST_FORWARD FOR 
	SELECT	 [SourceFile] = [FullPath], [XmlPath] 
	FROM	 [dbo].[AllPendingInspections](@SCUID, @InspectionID, @FullPath, @IsDirectory) 
	ORDER BY	[FullPath] ASC; 

	OPEN [FileCursor]; 

	IF(@@CURSOR_ROWS = 0)
	BEGIN 
		IF (@Debug = 1) 
			EXECUTE [Debug].[Write] @Indent=3, @Text=N'No Xml file(s) to process.'; 
	END 
	ELSE
	BEGIN
		FETCH NEXT FROM [FileCursor] INTO @SourceFile, @XmlPath; 
		WHILE (@@FETCH_STATUS = 0) 
		BEGIN 

			SET @FileName = [Utility].[GetFileName](@SourceFile); 

			IF (@Debug = 1) EXECUTE [Debug].[Write] @Indent=3, @Text=N'Assigned-Parameter: @SourceFile = ', @NVARCHAR=@SourceFile; 
			IF (@Debug = 1) EXECUTE [Debug].[Write] @Indent=3, @Text=N'Assigned-Parameter: @XmlPath = ', @NVARCHAR=@XmlPath; 

			BEGIN TRY 

				EXECUTE [dbo].[GetDefects] @Debug, @InspectionID, @SourceFile, @XmlPath; 

				EXECUTE [dbo].[GetLites] @Debug, @InspectionID, @SourceFile, @XmlPath; 

				EXECUTE [dbo].[GetInspectionFooter] @Debug, @InspectionID, @SourceFile, @XmlPath;

				--IF (SELECT [InspectionEndTime] FROM [dbo].[Inspections] WHERE [Id]=@InspectionID) = NULL
				--	DELETE [dbo].[PendingInspections] WHERE [InspectionId]=@InspectionID

			END TRY 
			BEGIN CATCH 

				DECLARE @ErrorNumber    AS INT; 
				DECLARE @ErrorSeverity  AS INT; 
				DECLARE @ErrorState     AS INT; 
				DECLARE @ErrorLine      AS INT; 
				DECLARE @ErrorProcedure AS NVARCHAR(200); 
				DECLARE @ErrorMessage   AS NVARCHAR(4000); 

				SET @ErrorNumber    = ERROR_NUMBER(); 
				SET @ErrorSeverity  = ERROR_SEVERITY(); 
				SET @ErrorState     = ERROR_STATE(); 
				SET @ErrorLine      = ERROR_LINE(); 
				SET @ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-'); 
				SET @ErrorMessage   = [dbo].[GetErrorString](@ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorLine, @ErrorProcedure, ERROR_MESSAGE()); 

				IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION; 

				EXECUTE [dbo].[ProcessError] @SCUID, @InspectionID, @SourceFile, @ErrorNumber, @ErrorMessage; 

				RAISERROR ('*** ERROR *** Check the error table', 0, 1, @FullPath) WITH NOWAIT; 
				RAISERROR (@ErrorMessage, @ErrorSeverity, 1); 

			END CATCH 

			FETCH NEXT FROM [FileCursor] INTO @SourceFile, @XmlPath; 
		END; 
		CLOSE [FileCursor]; 
		DEALLOCATE [FileCursor]; 
	END; 
	IF (@Debug = 1) EXECUTE [Debug].[Write] @Indent=2, @Text=N'Completed!'; 
END; 

