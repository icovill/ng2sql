﻿/*================================================================================================================
Script: dbo.GetInspectionHeader.sql

Synopsis: 

Notes:

==================================================================================================================
Revision History:

Date			Author				Description
------------------------------------------------------------------------------------------------------------------
09/06/2017		Ian Covill			Script Created

==================================================================================================================*/
CREATE PROCEDURE [dbo].[GetInspectionFooter]
( 
	@Debug AS BIT = 0, 
	@InspectionID AS INT, 
	@SourceFile AS NVARCHAR(255), 
	@XmlPath AS NVARCHAR(64) 
) 
AS 
BEGIN 
	---------------------------------------------------------------------------------------------------
	--// SET STATEMENTS                                                                            //--
	---------------------------------------------------------------------------------------------------
	SET NOCOUNT ON; 
	SET QUOTED_IDENTIFIER ON; 
	SET ARITHABORT OFF; 
	SET NUMERIC_ROUNDABORT OFF; 
	SET ANSI_WARNINGS ON; 
	SET ANSI_PADDING ON; 
	SET ANSI_NULLS ON; 
	SET CONCAT_NULL_YIELDS_NULL ON; 
	SET CURSOR_CLOSE_ON_COMMIT OFF; 
	SET IMPLICIT_TRANSACTIONS OFF; 
	SET DATEFORMAT MDY; 
	SET DATEFIRST 7; 
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; 

	---------------------------------------------------------------------------------------------------
	--// DECLARATIONS                                                                              //--
	---------------------------------------------------------------------------------------------------
	DECLARE @XmlHandle      AS INT; 
	DECLARE @XmlText        AS XML; 
	DECLARE @SQLString      AS NVARCHAR(1000); 
	DECLARE @ParmDefinition AS NVARCHAR(200); 
	DECLARE @DefectID       AS INT; 
	DECLARE @SourceFileID   AS INT; 
	DECLARE @IdentityOutput AS TABLE ([ID] INT); 

	IF (@Debug = 1) EXECUTE [Debug].[Write] @Indent=3, @Text=N'Executing: [dbo].[GetInspectionFooter]...'; 
	IF (@Debug = 1) EXECUTE [Debug].[Write] @Indent=4, @Text=N''; 

	---------------------------------------------------------------------------------------------------
	--// IMPORT XML USING OPENROWSET                                                               //--
	---------------------------------------------------------------------------------------------------
	SET @SQLString = N'SELECT @XmlText = CONVERT(XML, [BulkColumn]) FROM OPENROWSET(BULK ''' + @SourceFile + ''', SINGLE_BLOB) AS [Source];'; 

	SET @ParmDefinition = N'@XmlText XML OUTPUT'; 

	EXECUTE [dbo].[sp_executesql] @SQLString, @ParmDefinition, @XmlText OUTPUT; 

	---------------------------------------------------------------------------------------------------
	--// GET EXISTING OR CREATE NEW FILE                                                           //--
	---------------------------------------------------------------------------------------------------
	--EXECUTE [dbo].[GetSourceFileID] @Debug, @RollID, @SourceFile, @SourceFileID OUTPUT; 

	---------------------------------------------------------------------------------------------------
	--// INSERT USING OPENXML                                                                      //--
	---------------------------------------------------------------------------------------------------
	EXECUTE [dbo].[sp_xml_preparedocument] @XmlHandle OUTPUT, @XmlText; 

	IF (@XmlPath <> N'/Inspection')
		SET @XmlPath = N'/Inspection'; 


	UPDATE	 [dbo].[Inspections] SET 
			 [Length]				= [xml].[Length]
			,[Width]				= [xml].[Width]
			,[DefectCount]			= [xml].[DefectCount]
			,[InspectionEndTime]	= ISNULL([xml].[InspectionEndTime],NULL)
	FROM	OPENXML(@XmlHandle, @XmlPath, 1) 
			WITH (
				 [Length]				FLOAT		'Length'
				,[Width]				FLOAT		'Width'
				,[DefectCount]			INT			'DefectCount'
				,[InspectionEndTime]	DATETIME	'InspectionEndTime'
				) AS [xml]
	WHERE [Id] = @InspectionID; 

	EXECUTE [dbo].[sp_xml_removedocument] @XmlHandle; 

	IF (@Debug = 1) EXECUTE [Debug].[Write] @Indent=3, @Text=N'Completed!'; 

END;