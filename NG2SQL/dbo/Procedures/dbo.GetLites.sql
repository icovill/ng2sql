﻿/*================================================================================================================
Script: dbo.ShredXmlDefect.sql

Synopsis: 

Notes:

==================================================================================================================
Revision History:

Date			Author				Description
------------------------------------------------------------------------------------------------------------------
09/07/2017		Ian Covill			Script Created

==================================================================================================================*/
CREATE PROCEDURE [dbo].[GetLites]
( 
	@Debug AS BIT = 0, 
	@InspectionID AS INT, 
	@SourceFile AS NVARCHAR(255), 
	@XmlPath AS NVARCHAR(64) 
) 
AS 
BEGIN 
	---------------------------------------------------------------------------------------------------
	--// SET STATEMENTS                                                                            //--
	---------------------------------------------------------------------------------------------------
	SET NOCOUNT ON; 
	SET QUOTED_IDENTIFIER ON; 
	SET ARITHABORT OFF; 
	SET NUMERIC_ROUNDABORT OFF; 
	SET ANSI_WARNINGS ON; 
	SET ANSI_PADDING ON; 
	SET ANSI_NULLS ON; 
	SET CONCAT_NULL_YIELDS_NULL ON; 
	SET CURSOR_CLOSE_ON_COMMIT OFF; 
	SET IMPLICIT_TRANSACTIONS OFF; 
	SET DATEFORMAT MDY; 
	SET DATEFIRST 7; 
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; 

	---------------------------------------------------------------------------------------------------
	--// DECLARATIONS                                                                              //--
	---------------------------------------------------------------------------------------------------
	DECLARE @XmlHandle      AS INT; 
	DECLARE @XmlText        AS XML; 
	DECLARE @SQLString      AS NVARCHAR(1000); 
	DECLARE @ParmDefinition AS NVARCHAR(200); 
	DECLARE @LiteID       AS INT; 
	DECLARE @SourceFileID   AS INT; 
	DECLARE @IdentityOutput AS TABLE ([LiteID] INT, [LiteDefects] INT); 

	IF (@Debug = 1) EXECUTE [Debug].[Write] @Indent=3, @Text=N'Executing: [dbo].[GetLites]...'; 
	IF (@Debug = 1) EXECUTE [Debug].[Write] @Indent=4, @Text=N''; 

	---------------------------------------------------------------------------------------------------
	--// IMPORT XML USING OPENROWSET                                                               //--
	---------------------------------------------------------------------------------------------------
	SET @SQLString = N'SELECT @XmlText = CONVERT(XML, [BulkColumn]) FROM OPENROWSET(BULK ''' + @SourceFile + ''', SINGLE_BLOB) AS [Source];'; 

	SET @ParmDefinition = N'@XmlText XML OUTPUT'; 

	EXECUTE [dbo].[sp_executesql] @SQLString, @ParmDefinition, @XmlText OUTPUT; 

	---------------------------------------------------------------------------------------------------
	--// GET EXISTING OR CREATE NEW FILE                                                           //--
	---------------------------------------------------------------------------------------------------
	--EXECUTE [dbo].[GetSourceFileID] @Debug, @RollID, @SourceFile, @SourceFileID OUTPUT; 

	---------------------------------------------------------------------------------------------------
	--// INSERT USING OPENXML                                                                      //--
	---------------------------------------------------------------------------------------------------
	EXECUTE [dbo].[sp_xml_preparedocument] @XmlHandle OUTPUT, @XmlText; 

	IF (@XmlPath <> N'/Lite')
		SET @XmlPath = N'Inspection/Lites/Lite'; 
DECLARE @LiteDefectCount AS INT, @DefectSave AS INT

INSERT INTO [dbo].[Lites]
           (
		    [Index]
           ,[CleanNumber]
           ,[LiteArea]
           ,[LiteDefects]
		   ,[InspectionID]
		   )
	OUTPUT	 INSERTED.[Id], INSERTED.[LiteDefects] INTO @IdentityOutput
	SELECT	 [LiteIndex]         = [xml].[Index]
			,[CleanNumber]		 = [xml].[CleanNumber]
			,[LiteArea]			 = [xml].[LiteArea]
			,[LiteDefects]       = [xml].[LiteDefects]
			,[InspectionId]		 = @InspectionID
	FROM	 OPENXML(@XmlHandle,  N'Inspection/Lites/Lite/LiteInfo', 1) 
			 WITH 
			 ( 
				 [Index]       INT          'Index' 
				,[CleanNumber] FLOAT		'CleanNumber'
				,[LiteArea]	   FLOAT		'LiteArea'
				,[LiteDefects] INT		    'LiteDefects'
			 ) 
			 AS [xml]; 
	
	SET @LiteID = (SELECT TOP(1) [LiteID] FROM @IdentityOutput);
	SET @LiteDefectCount = (SELECT TOP(1) [LiteDefects] FROM @IdentityOutput);
	DECLARE @DebugVar AS INT

INSERT INTO [dbo].[Corners]
           (
            [CD]
           ,[MD]
           ,[LiteID]
		   )
	SELECT	 [CD]				 = [xml].[CD]
			,[MD]				 = [xml].[MD]
			,[LiteID]			 = @LiteID
	FROM	 OPENXML(@XmlHandle,  N'Inspection/Lites/Lite/Corners/Corner', 1) 
			 WITH 
			 ( 
				 [CD]		FLOAT		'CD'
				,[MD]	    FLOAT		'MD'
			 ) 
			 AS [xml]; 
	
	IF @LiteDefectCount>0
	BEGIN

		INSERT INTO [dbo].[LiteDefects]
           ([LiteID]
           ,[DefectSaveIndex])
		SELECT [LiteID]				=	@LiteID
			  ,[DefectSaveIndex]	=	CASE WHEN [xml].[DefectSaveIndex] IS NULL THEN -1 ELSE (SELECT TOP(1) [Id] FROM [dbo].[Defects] WHERE [InspectionID]=@InspectionID AND [DefectSaveIndex]=[xml].[DefectSaveIndex]) END
		FROM	 OPENXML(@XmlHandle,  N'Inspection/Lites/Lite/LiteDefects', 1) 
			 WITH 
			(
				[DefectSaveIndex]		INT		'DefectSaveIndex'
			)
			AS [xml]

	END
	EXECUTE [dbo].[sp_xml_removedocument] @XmlHandle; 

	IF (@Debug = 1) EXECUTE [Debug].[Write] @Indent=3, @Text=N'Completed!'; 
END;


GO
