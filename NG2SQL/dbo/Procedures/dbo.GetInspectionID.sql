﻿CREATE PROCEDURE [dbo].[GetInspectionID]
( 
	@Debug AS BIT = 0, 
	@SCUID int,
	@FullPath AS NVARCHAR(255), 
	@IsDirectory AS BIT, 
	@InspectionID AS INT OUTPUT 
) 
AS 
BEGIN 
	---------------------------------------------------------------------------------------------------
	--// SET STATEMENTS                                                                            //--
	---------------------------------------------------------------------------------------------------
	SET NOCOUNT ON; 
	SET QUOTED_IDENTIFIER ON; 
	SET ARITHABORT OFF; 
	SET NUMERIC_ROUNDABORT OFF; 
	SET ANSI_WARNINGS ON; 
	SET ANSI_PADDING ON; 
	SET ANSI_NULLS ON; 
	SET CONCAT_NULL_YIELDS_NULL ON; 
	SET CURSOR_CLOSE_ON_COMMIT OFF; 
	SET IMPLICIT_TRANSACTIONS OFF; 
	SET DATEFORMAT MDY; 
	SET DATEFIRST 7; 
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; 

	---------------------------------------------------------------------------------------------------
	--// DECLARATIONS                                                                              //--
	---------------------------------------------------------------------------------------------------
	DECLARE @InspectionString AS NVARCHAR(255),
	@InspectionStartTime DATETIME,
	@Recipe AS NVARCHAR(255),
	@ProductCode AS NVARCHAR(255), 
	@SourceFileID   AS INT;

	IF (@Debug = 1) EXECUTE [Debug].[Write] @Indent=1, @Text=N'Executing: [dbo].[GetInspectionID]...'; 
	IF (@Debug = 1) EXECUTE [Debug].[Write] @Indent=2, @Text=N'Input-Parameter: @FullPath = ', @NVARCHAR=@FullPath; 
	IF (@Debug = 1) EXECUTE [Debug].[Write] @Indent=2, @Text=N'Input-Parameter: @IsDirectory = ', @BIT=@IsDirectory; 

	EXECUTE [dbo].[GetInspectionHeader] 
		 @Debug
		,@SCUID
		,@IsDirectory
		,@FullPath OUTPUT
		,@InspectionString OUTPUT
		,@InspectionStartTime OUTPUT
		,@Recipe OUTPUT
		,@ProductCode OUTPUT; 

	IF (@Debug = 1) EXECUTE [Debug].[Write] @Indent=2, @Text=N'Output-Parameter: @FullPath = ', @NVARCHAR=@FullPath; 
	IF (@Debug = 1) EXECUTE [Debug].[Write] @Indent=2, @Text=N'Output-Parameter: @InspectionStartTime = ', @DATETIME=@InspectionStartTime; 

	IF (@InspectionStartTime IS NOT NULL)
	BEGIN TRY 

		SET	@InspectionID = 
		( 
			SELECT  [Inspections].[InspectionName] 
			FROM	[dbo].[Inspections] INNER JOIN
					[dbo].[PendingInspections] ON [Inspections].[Id]=[PendingInspections].[InspectionID]
			WHERE	[InspectionStartTime] = @InspectionStartTime
					AND [InspectionPath] = [dbo].[GetDataPath](@SCUID, @FullPath)
					AND [SCUID] = @SCUID
		); 

		IF (@InspectionID IS NULL) 
		BEGIN 

			EXECUTE [dbo].[CreateInspectionID] @Debug, @SCUID, @FullPath, @InspectionString, @InspectionStartTime, @Recipe, @ProductCode, @InspectionID OUTPUT;

			--EXECUTE [dbo].[CreateSourceFileID] @Debug, @InspectionID, @FullPath, @SourceFileID OUTPUT; 

			--EXECUTE [dbo].[CreateCommonDataID] @Debug, @InspectionID, @SourceFileID, @InspectionStartTime; 
		END; 

	END TRY 
	BEGIN CATCH 

		DECLARE @ErrorNumber    AS INT; 
		DECLARE @ErrorSeverity  AS INT; 
		DECLARE @ErrorState     AS INT; 
		DECLARE @ErrorLine      AS INT; 
		DECLARE @ErrorProcedure AS NVARCHAR(200); 
		DECLARE @ErrorMessage   AS NVARCHAR(4000); 

		SET @ErrorNumber    = ERROR_NUMBER(); 
		SET @ErrorSeverity  = ERROR_SEVERITY(); 
		SET @ErrorState     = ERROR_STATE(); 
		SET @ErrorLine      = ERROR_LINE(); 
		SET @ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-'); 
		SET @ErrorMessage   = [dbo].[GetErrorString](@ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorLine, @ErrorProcedure, ERROR_MESSAGE()); 

		IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION; 

		EXECUTE [dbo].[ProcessError] @SCUID, @FullPath, @ErrorNumber, @ErrorMessage; 

		RAISERROR ('*** ERROR *** Check the error table', 0, 1) WITH NOWAIT; 
		RAISERROR (@ErrorMessage, @ErrorSeverity, 1); 

	END CATCH 

	IF (@Debug = 1) EXECUTE [Debug].[Write] @Indent=2, @Text=N'Parameter: @RollID = ', @INT=@InspectionID; 
	IF (@Debug = 1) EXECUTE [Debug].[Write] @Indent=1, @Text=N'Completed!'; 
END; 

