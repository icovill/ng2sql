﻿CREATE FUNCTION [dbo].[GetConfigValue]
( 
	@ConfigID VARCHAR(64)
) 
RETURNS VARCHAR(1024) 
AS 
BEGIN 

	RETURN (SELECT TOP 1 [Value] FROM [dbo].[Config] WHERE [ConfigID] = @ConfigID)
END;