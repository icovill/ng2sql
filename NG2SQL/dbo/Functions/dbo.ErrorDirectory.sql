﻿CREATE FUNCTION [dbo].[ErrorDirectory]
( 
	@SCUID INT, 
	@FolderPath NVARCHAR(255) = NULL
) 
RETURNS NVARCHAR(255) 
AS 
BEGIN 
	RETURN [dbo].[GetInspectionDirectory](@SCUID, '\error' + ISNULL(@FolderPath, N'\')); 
END; 
