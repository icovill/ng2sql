﻿CREATE FUNCTION [dbo].[ArchiveDirectory]
( 
	@SCUID INT,
	@FolderPath NVARCHAR(255) = NULL
) 
RETURNS NVARCHAR(255) 
AS 
BEGIN 
	RETURN [dbo].[GetInspectionDirectory](@SCUID, '\archive' + ISNULL(@FolderPath, N'\')); 
END; 

