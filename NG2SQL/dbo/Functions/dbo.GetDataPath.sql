﻿CREATE FUNCTION [dbo].[GetDataPath]
( 
	@SCUID INT,
	@FullPath NVARCHAR(255)
) 
RETURNS NVARCHAR(255) 
AS 
BEGIN 
	DECLARE @FileName AS NVARCHAR(128); 
	DECLARE @DataDirectory AS NVARCHAR(255); 
	DECLARE @DataPath AS NVARCHAR(255);

	SET @FileName = [Utility].[GetFileName](@FullPath); 
	SET @DataDirectory = [dbo].[DataDirectory](@SCUID, N'\'); 

	SET @DataPath = REPLACE(REPLACE(@FullPath, @DataDirectory, N'\'), N'\' + @FileName, N''); 

	IF (LEFT(@DataPath, 1) <> N'\') SET @DataPath = N'\' + @DataPath; 

	RETURN @DataPath; 
END; 

