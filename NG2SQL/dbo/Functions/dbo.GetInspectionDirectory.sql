﻿CREATE FUNCTION [dbo].[GetInspectionDirectory]
(
	@SCUID int,
	@FolderPath NVARCHAR(255) = NULL
)
RETURNS NVARCHAR(255) 
AS 
BEGIN 
	DECLARE @XmlDirectory AS NVARCHAR(255); 

	-- @XmlDirectory should NOT include trailing slash 
	-- local service account must be given access to this directory 
	
	SET @XmlDirectory =  (SELECT [InspectionFolder] FROM [dbo].[SCUs] WHERE [Id] = @SCUID) -- update to reflect multi-machine set-up MM

	RETURN REPLACE(@XmlDirectory + ISNULL(@FolderPath, N'\') + N'\', N'\\', N'\');
END; 

