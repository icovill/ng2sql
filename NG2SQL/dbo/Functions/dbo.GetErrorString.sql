﻿CREATE FUNCTION [dbo].[GetErrorString]
(
	@ErrorNumber INT, 
	@ErrorSeverity INT, 
	@ErrorState INT, 
	@ErrorLine INT, 
	@ErrorProcedure NVARCHAR(200),
	@ErrorMessage NVARCHAR(4000)
)
RETURNS NVARCHAR(4000) 
AS 
BEGIN 
	DECLARE @String AS NVARCHAR(4000);

	SET @String = N'';

	SET @String = @String + N'Error ' + CAST(@ErrorNumber AS NVARCHAR) + N', ';
	SET @String = @String + N'Level ' + CAST(@ErrorSeverity AS NVARCHAR) + N', '; 
	SET @String = @String + N'State ' + CAST(@ErrorState AS NVARCHAR) + N', '; 
	SET @String = @String + N'Procedure ' + @ErrorProcedure + N', '; 
	SET @String = @String + N'Line ' + CAST(@ErrorLine AS NVARCHAR) + N', '; 
	SET @String = @String + N'Message: ' + @ErrorMessage;

	RETURN @String;
END; 
