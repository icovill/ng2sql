﻿CREATE FUNCTION [dbo].[DataDirectory]
( 
	@SCUID INT,
	@FolderPath NVARCHAR(255) = NULL
) 
RETURNS NVARCHAR(255) 
AS 
BEGIN 
	RETURN [dbo].[GetInspectionDirectory](@SCUID, '\data' + ISNULL(@FolderPath, N'\')); 
END; 

