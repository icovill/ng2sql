﻿CREATE FUNCTION [dbo].[AllPendingInspections]
( 
	@SCUID INT,
	@InspectionID AS INT,
	@FullPath AS NVARCHAR(255), 
	@IsDirectory AS BIT 
) 
RETURNS @RankedResults TABLE 
(
	[Rank] INT, 
	[FullPath] NVARCHAR(255), 
	[XmlPath] NVARCHAR(64) 
)
AS 
BEGIN 
	INSERT INTO @RankedResults ([FullPath], [XmlPath])
	SELECT	 [PendingFile].[FullPath]
			,[PendingFile].[XmlPath] 
	FROM	 [dbo].[PossibleLocations](@FullPath, @IsDirectory) AS [PendingFile]
			 LEFT OUTER JOIN
			 (
				SELECT	[FullPath] = @FullPath + N'Roll.xml'
				FROM	[dbo].[Inspections] AS [Inspections] 
				WHERE	[Inspections].[Id] = @InspectionID 
						AND [Width] IS NOT NULL 
						AND [InspectionEndTime] IS NOT NULL 
				UNION ALL 
				SELECT	[FullPath] = [dbo].[DataDirectory](@SCUID,[DataPath]) + N'Roll.xml'
				FROM	[dbo].[Defects]  AS [Defects]  
						INNER JOIN [dbo].[Inspections] AS [Inspections]
								ON [Inspections].[Id] = [Defects].[InspectionID]
				WHERE	[Defects].[InspectionID] = @InspectionID 
			 ) 
			 AS [CompletedFile] 
					ON [CompletedFile].[FullPath] = [PendingFile].[FullPath]
	WHERE	[CompletedFile].[FullPath] IS NULL; 

	RETURN;
END; 

