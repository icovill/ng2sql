﻿CREATE FUNCTION [dbo].[PossibleLocations]
( 
	@FullPath AS NVARCHAR(255), 
	@IsDirectory AS BIT 
) 
RETURNS @RankedResults TABLE 
(
	[FullPath] NVARCHAR(255), 
	[XmlPath] NVARCHAR(64) 
)
AS 
BEGIN 
	IF (@IsDirectory = 1) 
	BEGIN 
		;WITH [Query] AS 
		(
			SELECT	 [FullPath], [XmlPath] = N'/Inspection' 
			FROM	 [Utility].[DirectoryList](@FullPath, '*.xml') 
			WHERE	 [Name] LIKE N'roll%' 
		)
		INSERT INTO @RankedResults ([FullPath], [XmlPath])
		SELECT [FullPath], [XmlPath]
		FROM [Query] 
	END
	ELSE
	BEGIN
		INSERT INTO @RankedResults ([FullPath], [XmlPath])
		SELECT [FullPath] = @FullPath, [XmlPath] = N'/Inspection'
	END; 

	RETURN;
END; 

