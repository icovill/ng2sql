﻿CREATE TABLE [dbo].[LiteDefects]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [DefectSaveIndex] INT NULL, 
    [LiteID] INT NULL, 
    CONSTRAINT [FK_LiteDefects_Lites] FOREIGN KEY (LiteID) REFERENCES [Lites]([Id]) 
)
