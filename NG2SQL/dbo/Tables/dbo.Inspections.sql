﻿CREATE TABLE [dbo].[Inspections]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [InspectionName] NVARCHAR(50) NULL, 
    [InspectionStartTime] DATETIME2 NULL, 
    [Recipe] NVARCHAR(50) NULL, 
    [ProductCode] NVARCHAR(50) NULL, 
    [Length] FLOAT NULL, 
    [Width] FLOAT NULL, 
    [DefectCount] FLOAT NULL, 
    [InspectionEndTime] DATETIME NULL, 
    [TimeStamp] DATETIME NULL, 
    [DataPath] NVARCHAR(50) NULL, 
    [ArchivePath] NVARCHAR(50) NULL, 
    [ErrorOccured] BIT NULL, 
    [SCUID] INT NULL, 
    CONSTRAINT [FK_Inspections_SCUs] FOREIGN KEY ([SCUID]) REFERENCES [SCUs]([Id])
)
