﻿CREATE TABLE [dbo].[Lites]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Index] INT NULL, 
    [CleanNumber] FLOAT NULL, 
    [LiteArea] FLOAT NULL, 
    [LiteDefects] INT NULL, 
    [InspectionID] INT NULL, 
    CONSTRAINT [FK_Lites_Inspections] FOREIGN KEY ([InspectionID]) REFERENCES [Inspections]([Id])
)
