﻿CREATE TABLE [dbo].[Errors]
(
	[Id] INT IDENTITY(1,1) NOT NULL PRIMARY KEY, 
    [ErrorMessage] NVARCHAR(MAX) NULL, 
    [TimeStamp] DATETIME NULL, 
    [InspectionID] INT NULL, 
)