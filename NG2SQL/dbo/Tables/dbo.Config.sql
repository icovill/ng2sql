﻿CREATE TABLE [dbo].[Config](
    [ConfigID] VARCHAR (64)   NOT NULL,
    [Value]    VARCHAR (1024) NOT NULL,
    [Active]   BIT            CONSTRAINT [DF__Config__Active] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK__Config__ConfigID] PRIMARY KEY CLUSTERED ([ConfigID] ASC) WITH (FILLFACTOR = 95)
);

