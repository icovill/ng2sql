﻿CREATE TABLE [dbo].[FactoryInputs]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [InputName] NVARCHAR(255) NULL, 
    [InputValue] NVARCHAR(255) NULL, 
    [InspectionId] INT NOT NULL, 
    CONSTRAINT [FK_FactoryInputs_Inspections] FOREIGN KEY ([InspectionId]) REFERENCES [Inspections]([Id]) 

)
