﻿CREATE TABLE [dbo].[SCUs]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [SCUName] NVARCHAR(50) NULL, 
    [NetworkAddress] NVARCHAR(50) NULL, 
    [InspectionFolder] NVARCHAR(50) NULL, 
    [Active] BIT NULL
)
