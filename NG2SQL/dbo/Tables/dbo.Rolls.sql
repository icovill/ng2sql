﻿CREATE TABLE [dbo].[Rolls]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [RollId] NCHAR(10) NULL,
    [Length] FLOAT NULL, 
    [Width] FLOAT NULL, 
    [TimeStamp] DATETIME NULL, 
    [RollName] NVARCHAR(50) NULL, 
    [InspectionID] INT NULL, 
    CONSTRAINT [FK_Roll_Inspections] FOREIGN KEY (InspectionID) REFERENCES [Inspections]([Id])
)
