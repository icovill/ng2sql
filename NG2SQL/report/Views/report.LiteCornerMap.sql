﻿
CREATE VIEW [report].[LiteCornerMap]
AS
SELECT        ISNULL(dbo.Inspections.InspectionName, dbo.Inspections.InspectionStartTime) AS Inspection, dbo.Lites.[Index], dbo.Corners.CD, dbo.Corners.MD
FROM            dbo.Inspections INNER JOIN
                         dbo.Lites ON dbo.Inspections.Id = dbo.Lites.InspectionID INNER JOIN
                         dbo.Corners ON dbo.Lites.Id = dbo.Corners.LiteID