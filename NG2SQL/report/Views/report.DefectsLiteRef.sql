﻿CREATE VIEW report.DefectsLiteRef
AS
SELECT        TOP (100) PERCENT ISNULL(dbo.Inspections.InspectionName, dbo.Inspections.InspectionStartTime) AS Inspection, dbo.Lites.[Index], dbo.Defects.CDPos -
                             (SELECT        (MAX(CD) + MIN(CD)) / 2 AS Expr1
                               FROM            dbo.Corners
                               WHERE        (dbo.Lites.Id = LiteID)) AS TrueCDPos, dbo.Defects.MDPos -
                             (SELECT        (MAX(MD) + MIN(MD)) / 2 AS Expr1
                               FROM            dbo.Corners
                               WHERE        (dbo.Lites.Id = LiteID)) AS TrueMDPos, dbo.Defects.ClassName
FROM            dbo.Lites INNER JOIN
                         dbo.Inspections ON dbo.Lites.InspectionID = dbo.Inspections.Id INNER JOIN
                         dbo.LiteDefects ON dbo.Lites.Id = dbo.LiteDefects.LiteID INNER JOIN
                         dbo.Defects ON dbo.LiteDefects.DefectSaveIndex = dbo.Defects.DefectSaveIndex AND dbo.Defects.InspectionID = dbo.Inspections.Id
ORDER BY Inspection, dbo.Lites.[Index], dbo.Defects.DefectSaveIndex
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'report', @level1type = N'VIEW', @level1name = N'DefectsLiteRef';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Lites"
            Begin Extent = 
               Top = 64
               Left = 32
               Bottom = 225
               Right = 202
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Inspections"
            Begin Extent = 
               Top = 7
               Left = 272
               Bottom = 136
               Right = 467
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "LiteDefects"
            Begin Extent = 
               Top = 140
               Left = 270
               Bottom = 252
               Right = 445
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Defects"
            Begin Extent = 
               Top = 59
               Left = 535
               Bottom = 215
               Right = 728
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'report', @level1type = N'VIEW', @level1name = N'DefectsLiteRef';

