﻿
CREATE VIEW [report].[LiteAndDefectMap]
AS
SELECT        ISNULL(dbo.Inspections.InspectionName, dbo.Inspections.InspectionStartTime) AS Inspection, dbo.Lites.[Index], dbo.Corners.CD, dbo.Corners.MD, 
                         dbo.Defects.CDPos, dbo.Defects.MDPos
FROM            dbo.Inspections INNER JOIN
                         dbo.Defects ON dbo.Inspections.Id = dbo.Defects.InspectionID INNER JOIN
                         dbo.Lites ON dbo.Inspections.Id = dbo.Lites.InspectionID INNER JOIN
                         dbo.Corners ON dbo.Lites.Id = dbo.Corners.LiteID