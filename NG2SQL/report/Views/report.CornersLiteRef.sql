﻿


CREATE VIEW [report].[CornersLiteRef]
AS
SELECT        TOP (100) PERCENT ISNULL(dbo.Inspections.InspectionName, dbo.Inspections.InspectionStartTime) AS Inspection, Lites.[Index], Corners.CD -
                             (SELECT        (MAX(dbo.Corners.CD)+MIN(dbo.Corners.CD))/2 AS Expr1
                               FROM            dbo.Corners WHERE
                                                         dbo.Lites.Id = dbo.Corners.LiteID) AS TrueCDPos, Corners.MD -
                             (SELECT        (MAX(Corners.MD)+MIN(Corners.MD))/2 AS Expr1
                               FROM            dbo.Corners WHERE
                                                         dbo.Lites.Id = Corners.LiteID) AS TrueMDPos
FROM            dbo.Corners INNER JOIN
                         dbo.Lites ON Corners.LiteID = Lites.Id INNER JOIN
                         dbo.Inspections ON Lites.InspectionID = dbo.Inspections.Id
GROUP BY dbo.Inspections.InspectionName, dbo.Inspections.InspectionStartTime, Lites.[Index], Corners.CD, Corners.MD, Corners.Id, Lites.Id
ORDER BY Inspection, Lites.[Index], Corners.Id