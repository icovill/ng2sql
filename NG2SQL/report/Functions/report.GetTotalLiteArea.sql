﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [report].[GetTotalLiteArea]
( 
	@InspectionID int
) 
RETURNS FLOAT
AS
BEGIN
	RETURN (
	SELECT SUM([LiteArea]) 
	FROM [dbo].[Lites]
	WHERE [InspectionID]  = @InspectionID
	); 
END