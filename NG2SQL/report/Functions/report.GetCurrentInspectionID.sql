﻿CREATE FUNCTION [report].[GetCurrentInspectionID]
(@SCUID INT)
RETURNS NVARCHAR(255)
AS
BEGIN
	DECLARE @InspectionStartTime DATETIME = [report].[GetMaxInspectionStartTime](@SCUID)
	RETURN (
			SELECT	TOP 1 [InspectionName]
				FROM	[dbo].[Inspections] 
				WHERE [SCUID]  = @SCUID
					AND [InspectionStartTime] = @InspectionStartTime
)
END
