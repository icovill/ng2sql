﻿CREATE FUNCTION [report].[GetMaxInspectionStartTime]
( 
	@SCUID int
) 
RETURNS DATETIME
AS
BEGIN
	RETURN (
	SELECT MAX([InspectionStartTime]) 
	FROM [dbo].[Inspections]
	WHERE [SCUID]  = @SCUID
	); 
END
