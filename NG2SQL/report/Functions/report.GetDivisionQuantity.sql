﻿CREATE FUNCTION [report].[GetDivisionQuantity]
(
	@SCUID INT,
	@InspectionStartDate DATETIME, 
	@InspectionStartTime INT, 
	@InspectionEndDate DATETIME, 
	@InspectionEndTime INT,
	@DivisionCount AS INT, 
	@InspectionString AS NVARCHAR(64),
	@ProductCode AS NVARCHAR(64)
)
RETURNS INT
AS
BEGIN
	RETURN 	(
		SELECT	 [DivisionQuantity] = (MAX([Defects].[MDPos]) / 12) / (@DivisionCount - 1) 
		FROM	 [dbo].[Inspections]
					INNER JOIN [dbo].[Defects]
							ON [Defects].[InspectionID] = [Inspections].[Id] 
		WHERE	[SCUID] = @SCUID
		AND [InspectionStartTime] >= DATEADD(HOUR, @InspectionStartTime, CAST(@InspectionStartDate AS DATETIME)) 
		AND [InspectionEndTime] <= DATEADD(HOUR, @InspectionEndTime, CAST(@InspectionEndDate AS DATETIME)) 
		AND (([Inspections].[InspectionName]  = @InspectionString AND @InspectionString IS NOT NULL) OR (@InspectionString IS NULL))
		AND	(([ProductCode]  = @ProductCode AND @ProductCode IS NOT NULL) OR (@ProductCode IS NULL))
	); 
END;
