﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [report].[GetAvgLiteArea]
( 
	@InspectionID int
) 
RETURNS FLOAT
AS
BEGIN
	RETURN (
	SELECT AVG([LiteArea]) 
	FROM [dbo].[Lites]
	WHERE [InspectionID]  = @InspectionID
	); 
END