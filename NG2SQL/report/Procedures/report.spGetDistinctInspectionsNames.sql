﻿/*================================================================================================================ 
Script: dbo.spGetDistinctDefectClassForLoadNumber.sql 

Synopsis: 

Notes: 

================================================================================================================== 
Revision History: 

Date			Author				Description 
------------------------------------------------------------------------------------------------------------------ 
06/21/2016		Bob Stixrud			Script Created 
10/19/2016      Miro Maraz          Added MachineID parameter

==================================================================================================================*/ 
CREATE PROCEDURE [report].[spGetDistinctInspectionsNames]
( 
	@SCUID INT,
	@InspectionStartDate AS DATETIME, 
	@InspectionStartTime AS INT, 
	@InspectionEndDate AS DATETIME, 
	@InspectionEndTime AS INT 
) 
AS 
BEGIN 
	SET NOCOUNT ON; 
	SET QUOTED_IDENTIFIER ON; 
	SET ARITHABORT OFF; 
	SET NUMERIC_ROUNDABORT OFF; 
	SET ANSI_WARNINGS ON; 
	SET ANSI_PADDING ON; 
	SET ANSI_NULLS ON; 
	SET CONCAT_NULL_YIELDS_NULL ON; 
	SET CURSOR_CLOSE_ON_COMMIT OFF; 
	SET IMPLICIT_TRANSACTIONS OFF; 
	SET DATEFORMAT MDY; 
	SET DATEFIRST 7; 
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; 

	DECLARE @InspectionName NVARCHAR(64) = [dbo].[GetConfigValue]('InspectionName.All.Label')
	
	SELECT NULL as [InspectionName] , @InspectionName as Label
	UNION 

	SELECT [InspectionName], [InspectionName]
	FROM (
	SELECT	DISTINCT ISNULL(CASE WHEN [InspectionName] = '' THEN CAST([InspectionStartTime] AS NVARCHAR) ELSE [InspectionName] END, CAST([InspectionStartTime] AS NVARCHAR)) AS InspectionName
	FROM	[dbo].[Inspections]
	WHERE [SCUID]  = @SCUID
	      AND [InspectionStartTime] >= DATEADD(HOUR, @InspectionStartTime, CAST(@InspectionStartDate AS DATETIME)) 
		  AND [InspectionEndTime] <= DATEADD(HOUR, @InspectionEndTime, CAST(@InspectionEndDate AS DATETIME))
	) as LN
END; 

