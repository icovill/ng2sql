﻿/*================================================================================================================
Script: report.spLoadDefectsTimeInterval.sql

Synopsis: 

Notes:

==================================================================================================================
Revision History:

Date			Author				Description
------------------------------------------------------------------------------------------------------------------
06/21/2016		Bob Stixrud			Script Created
10/19/2016      Miro Maraz          Added MachineID parameter
09/08/2017		Ian Covill			Corrected for NxtGen2

==================================================================================================================*/
CREATE PROCEDURE [report].[spLoadDefectsTimeInterval]
(
	@SCUID INT,
	@InspectionStartDate DATETIME, 
	@InspectionStartTime INT, 
	@InspectionEndDate DATETIME, 
	@InspectionEndTime INT
)
AS
BEGIN
	SET NOCOUNT ON; 
	SET QUOTED_IDENTIFIER ON; 
	SET ARITHABORT OFF; 
	SET NUMERIC_ROUNDABORT OFF; 
	SET ANSI_WARNINGS ON; 
	SET ANSI_PADDING ON; 
	SET ANSI_NULLS ON; 
	SET CONCAT_NULL_YIELDS_NULL ON; 
	SET CURSOR_CLOSE_ON_COMMIT OFF; 
	SET IMPLICIT_TRANSACTIONS OFF; 
	SET DATEFORMAT MDY; 
	SET DATEFIRST 7; 
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; 

	SELECT	 (CASE WHEN [InspectionName]='' THEN CAST([Id] AS NVARCHAR) ELSE [InspectionName] END) AS InspectionName
			,[ClassName] 
			,[Count] = COUNT([DefectID]) 
	FROM	 [report].[InspectionDefects]
	WHERE [SCUID]  = @SCUID
			AND [InspectionStartTime] >= DATEADD(HOUR, @InspectionStartTime, CAST(@InspectionStartDate AS DATETIME)) 
			AND [InspectionEndTime] <= DATEADD(HOUR, @InspectionEndTime, CAST(@InspectionEndDate AS DATETIME)) 
	GROUP BY [InspectionName]
	,[Id] 
			,[ClassName]; 
END; 

