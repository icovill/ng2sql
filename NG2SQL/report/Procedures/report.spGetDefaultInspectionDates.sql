﻿CREATE PROCEDURE [report].[spGetDefaultInspectionDates]
( 
	@SCUID INT,
	@Days AS INT
) 
AS 
BEGIN 
	SET NOCOUNT ON; 
	SET QUOTED_IDENTIFIER ON; 
	SET ARITHABORT OFF; 
	SET NUMERIC_ROUNDABORT OFF; 
	SET ANSI_WARNINGS ON; 
	SET ANSI_PADDING ON; 
	SET ANSI_NULLS ON; 
	SET CONCAT_NULL_YIELDS_NULL ON; 
	SET CURSOR_CLOSE_ON_COMMIT OFF; 
	SET IMPLICIT_TRANSACTIONS OFF; 
	SET DATEFORMAT MDY; 
	SET DATEFIRST 7; 
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; 

	DECLARE @MaxInspectionStartTime DATETIME;

	SET @MaxInspectionStartTime = [report].[GetMaxInspectionStartTime](@SCUID); 

	SELECT	[InspectionStartDate] = CAST(MIN([InspectionStartTime]) AS DATE), 
			[InspectionStartTime] = DATEPART(HOUR, MIN([InspectionStartTime])), 
			[InspectionEndDate] = CAST(MAX([InspectionEndTime]) AS DATE), 
			[InspectionEndTime] = DATEPART(HOUR, MAX([InspectionEndTime])) +1
	FROM	[dbo].[Inspections]
	WHERE [SCUID] = @SCUID
	      AND [InspectionStartTime] > DATEADD(DAY, (ABS(@Days) * -1), @MaxInspectionStartTime); 
END; 

