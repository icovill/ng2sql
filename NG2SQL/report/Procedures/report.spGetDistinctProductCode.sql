﻿/*================================================================================================================ 
Script: report.spGetDistinctProductCode.sql 

Synopsis: 

Notes: 

================================================================================================================== 
Revision History: 

Date			Author				Description 
------------------------------------------------------------------------------------------------------------------ 
10/21/2016      Miro Maraz          Created

==================================================================================================================*/ 
CREATE PROCEDURE [report].[spGetDistinctProductCode]
( 
	@SCUID INT,
	@InspectionStartDate AS DATETIME, 
	@InspectionStartTime AS INT, 
	@InspectionEndDate AS DATETIME, 
	@InspectionEndTime AS INT 
) 
AS 
BEGIN 
	SET NOCOUNT ON; 
	SET QUOTED_IDENTIFIER ON; 
	SET ARITHABORT OFF; 
	SET NUMERIC_ROUNDABORT OFF; 
	SET ANSI_WARNINGS ON; 
	SET ANSI_PADDING ON; 
	SET ANSI_NULLS ON; 
	SET CONCAT_NULL_YIELDS_NULL ON; 
	SET CURSOR_CLOSE_ON_COMMIT OFF; 
	SET IMPLICIT_TRANSACTIONS OFF; 
	SET DATEFORMAT MDY; 
	SET DATEFIRST 7; 
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; 

	DECLARE @ProductCodeAll NVARCHAR(64) = [dbo].[GetConfigValue]('ProductCode.All.Label')

	SELECT NULL as [ProductCode] , @ProductCodeAll as Label
	UNION 
	SELECT	DISTINCT [ProductCode],[ProductCode]
	FROM	[dbo].[Inspections] 
	WHERE [SCUID]  = @SCUID
	        AND [InspectionStartTime] >= DATEADD(HOUR, @InspectionStartTime, CAST(@InspectionStartDate AS DATETIME))
			AND [InspectionEndTime] <= DATEADD(HOUR, @InspectionEndTime, CAST(@InspectionEndDate AS DATETIME)); 
END;