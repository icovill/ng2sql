﻿CREATE PROCEDURE [report].[spDefectsByTypePerCDDivision]
(
	@SCUID INT,
	@InspectionStartDate DATETIME, 
	@InspectionStartTime INT, 
	@InspectionEndDate DATETIME, 
	@InspectionEndTime INT,
	@DivisionCount AS INT, 
	@InspectionString AS NVARCHAR(64),
	@ProductCode AS NVARCHAR(64)
)
AS
BEGIN
	SET NOCOUNT ON;
	SET QUOTED_IDENTIFIER ON;
	SET ARITHABORT OFF;
	SET NUMERIC_ROUNDABORT OFF;
	SET ANSI_WARNINGS ON;
	SET ANSI_PADDING ON;
	SET ANSI_NULLS ON;
	SET CONCAT_NULL_YIELDS_NULL ON;
	SET CURSOR_CLOSE_ON_COMMIT OFF;
	SET IMPLICIT_TRANSACTIONS OFF;
	SET DATEFORMAT MDY;
	SET DATEFIRST 7;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; 

	DECLARE @DivisionQuantity AS DECIMAL(18, 2) = 
		[report].[GetDivisionQuantity]( 
	   @SCUID
	  ,@InspectionStartDate
	  ,@InspectionStartTime
	  ,@InspectionEndDate
	  ,@InspectionEndTime
	  ,@DivisionCount
	  ,@InspectionString
	  ,@ProductCode
	)

	IF (OBJECT_ID('tempdb..#REPORT_DATA') IS NOT NULL) 
	BEGIN 
		DROP TABLE [#REPORT_DATA]; 
	END; 

	CREATE TABLE [#REPORT_DATA] 
	(
		 [CDDistance] DECIMAL(18, 2) NOT NULL 
		,[Class] NVARCHAR(64) NOT NULL 
		,[Count] INT NOT NULL 
	);

	;WITH [DistinctClass]
	AS
	(
		SELECT	 DISTINCT [Class]
		FROM	 [report].[DefectClasses]
		WHERE	[SCUID] = @SCUID
		AND [InspectionStartTime] >= DATEADD(HOUR, @InspectionStartTime, CAST(@InspectionStartDate AS DATETIME)) 
		AND [InspectionEndTime] <= DATEADD(HOUR, @InspectionEndTime, CAST(@InspectionEndDate AS DATETIME)) 
		AND (([InspectionName]  = @InspectionString AND @InspectionString IS NOT NULL) OR (@InspectionString IS NULL))
		AND	 (([ProductCode]  = @ProductCode AND @ProductCode IS NOT NULL) OR (@ProductCode IS NULL))
	), 
	[RecursiveCTE] AS
	( 
		SELECT	 [Lane] = 1 
				,[CDDistance] = @DivisionQuantity 
				,[Class] 
				,[Count] = 0 
		FROM	 [DistinctClass] 

		UNION ALL 

		SELECT	 [Lane] = [RecursiveCTE].[Lane] + 1 
				,[CDDistance] = CAST([RecursiveCTE].[CDDistance] + @DivisionQuantity AS DECIMAL(18, 2)) 
				,[Class] = [RecursiveCTE].[Class] 
				,[Count] = [RecursiveCTE].[Count] 
		FROM	 [DistinctClass] 
				 INNER JOIN [RecursiveCTE] 
						 ON [RecursiveCTE].[Class] = [DistinctClass].[Class] 
		WHERE	 [RecursiveCTE].[Lane] + 1 < = @DivisionCount 
	) 
	INSERT INTO [#REPORT_DATA] 
	( 
			 [CDDistance], [Class], [Count] 
	) 
	SELECT	 [CDDistance], [Class], [Count] 
	FROM	 [RecursiveCTE] 
	ORDER BY [CDDistance], [Class] 

	;WITH [CTE] AS
	(
	SELECT	 [CDDistance] = (FLOOR(CAST([CDPos] / 12 AS DECIMAL(18, 2)) / @DivisionQuantity) * @DivisionQuantity) + @DivisionQuantity 
			,[ClassName] AS 'Class'
			,[DefectID]
	FROM	 [report].[InspectionDefects]
		WHERE	[SCUID] = @SCUID	
		    AND [InspectionStartTime] >= DATEADD(HOUR, @InspectionStartTime, CAST(@InspectionStartDate AS DATETIME)) 
		AND [InspectionEndTime] <= DATEADD(HOUR, @InspectionEndTime, CAST(@InspectionEndDate AS DATETIME)) 
		AND (([InspectionName]  = @InspectionString AND @InspectionString IS NOT NULL) OR (@InspectionString IS NULL))
		AND	 (([ProductCode]  = @ProductCode AND @ProductCode IS NOT NULL) OR (@ProductCode IS NULL))
	)
	SELECT	 [RPT].[CDDistance] 
			,[RPT].[Class] 
			,[Count] = COUNT([CTE].[DefectID]) 
	FROM	 [#REPORT_DATA] AS [RPT] 
			 LEFT OUTER JOIN [CTE] AS [CTE] 
					 ON [CTE].[CDDistance] = [RPT].[CDDistance] 
						AND [CTE].[Class] = [RPT].[Class] 
	GROUP BY [RPT].[CDDistance] 
			,[RPT].[Class] 
	ORDER BY [RPT].[CDDistance] 
			,[RPT].[Class]; 
END;