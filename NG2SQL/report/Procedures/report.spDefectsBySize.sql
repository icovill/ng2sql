﻿CREATE PROCEDURE [report].[spDefectsBySize]
( 
	@SCUID INT,
	@InspectionStartDate DATETIME, 
	@InspectionStartTime INT, 
	@InspectionEndDate DATETIME, 
	@InspectionEndTime INT,
	@ProductCode AS NVARCHAR(64)
) 
AS 
BEGIN 
	SET NOCOUNT ON; 
	SET QUOTED_IDENTIFIER ON; 
	SET ARITHABORT OFF; 
	SET NUMERIC_ROUNDABORT OFF; 
	SET ANSI_WARNINGS ON; 
	SET ANSI_PADDING ON; 
	SET ANSI_NULLS ON; 
	SET CONCAT_NULL_YIELDS_NULL ON; 
	SET CURSOR_CLOSE_ON_COMMIT OFF; 
	SET IMPLICIT_TRANSACTIONS OFF; 
	SET DATEFORMAT MDY; 
	SET DATEFIRST 7; 
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; 

	SELECT	 [Order]
			,[Count]
			,[Range]
	FROM	 [report].[DefectsBySize]	WHERE [SCUID]  = @SCUID
			AND [InspectionStartTime] >= DATEADD(HOUR, @InspectionStartTime, CAST(@InspectionStartDate AS DATETIME)) 
			AND [InspectionEndTime] <= DATEADD(HOUR, @InspectionEndTime, CAST(@InspectionEndDate AS DATETIME)) 
			AND	 (([ProductCode]  = @ProductCode AND @ProductCode IS NOT NULL) OR (@ProductCode IS NULL))
	 
END;