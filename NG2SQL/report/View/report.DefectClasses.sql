﻿CREATE VIEW [report].[DefectClasses]
	AS
	SELECT	 DISTINCT 
		 [Defects].[ClassName] AS [Class]
		,[SCUID]
		,[InspectionStartTime]
		,[InspectionEndTime]
		,[Inspections].[InspectionName]
		,[ProductCode]
		FROM	 [dbo].[Inspections] 
				 INNER JOIN [dbo].[Defects]
						ON [Defects].[InspectionID] = [Inspections].[Id]
