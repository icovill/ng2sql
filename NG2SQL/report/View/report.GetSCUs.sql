﻿CREATE VIEW [report].[GetSCUs]
	AS 	SELECT [Id], [SCUName] + ' (' + [NetworkAddress]   + ')' as [Label]
	FROM	[dbo].[SCUs]

