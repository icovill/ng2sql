﻿CREATE VIEW [report].[DefectsBySize]
	AS 
		SELECT TOP 100 PERCENT	 
		 [Size].[Order]
		,[Count] = COUNT([Defects].[Id])
		,[Size].[Range]
		,[SCUID]
		,[InspectionStartTime]
		,[InspectionEndTime]
		,[ProductCode]
	FROM	 [dbo].[Inspections]
				INNER JOIN [dbo].[Defects]
						ON [Defects].[InspectionID] = [Inspections].[Id] 
				INNER JOIN 
				(
				SELECT * FROM ( VALUES
				(1,'< 0.005', - 1.79E+308, 0.0055),
				(2,'0.006 - 0.010' ,0.055, 0.0105),
				(3,'0.011 - 0.020', 0.0105, 0.0205),
				(4,'0.021 - 0.050', 0.0205, 0.0505 ),
				(5,'> 0.050', 0.0505 , 1.79E+308 )) as Size([Order], [Range], [MinValue], [MaxValue])
				) as [Size]
			ON [Defects].[Area] >= Size.MinValue AND [Defects].[Area] < Size.[MaxValue]
	GROUP BY [Size].[Order] , [Size].[Range], [SCUID], [InspectionStartTime], [InspectionEndTime], [ProductCode]
	ORDER BY [Size].[Order] ASC;
