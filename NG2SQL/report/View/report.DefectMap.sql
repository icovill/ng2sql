﻿CREATE VIEW [report].[DefectMap]
	AS 
	SELECT TOP 100 PERCENT   
			 [CDPos]
			,[MDPos]
			,[ClassName] AS [Class]
			,[SCUID]
			,[InspectionStartTime]
			,[InspectionEndTime]
			,[Inspections].[InspectionName]
			,[ProductCode]
	FROM	 [dbo].[Defects]
			 INNER JOIN [dbo].[Inspections]
			 ON [Inspections].[Id] = [Defects].[InspectionID]
	ORDER BY [Defects].[CDPos];

