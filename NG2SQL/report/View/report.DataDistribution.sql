﻿CREATE VIEW [report].[DataDistribution]
	AS
	 	SELECT TOP 100 PERCENT [Date], [SCUs].[SCUName] + ' (' + [SCUs].[NetworkAddress] + ')' as [SCU], [InspectionName], [DefectCount]
	FROM (
	SELECT	 [Date] = CONVERT(VARCHAR(10), [InspectionStartTime], 101)
			,[Inspections].[InspectionName]
			,[DefectCount] = COUNT([Defects].[Id])
			,[SCUID]
	FROM	 [dbo].[Inspections]
			 INNER JOIN [dbo].[Defects]
					 ON [Defects].[InspectionID] = [Inspections].[Id] 
	GROUP BY CONVERT(VARCHAR(10), [InspectionStartTime], 101)
			,[Inspections].[InspectionName]
			,[Inspections].[SCUID]
	) as DC
	 INNER JOIN [dbo].[SCUs]
			ON [SCUs].[Id] = DC.[SCUID]
	ORDER BY [Date], [SCU], [InspectionName];




