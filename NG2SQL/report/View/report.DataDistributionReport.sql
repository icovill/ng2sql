﻿CREATE VIEW [report].[DataDistributionReport]
	AS 
	SELECT TOP 100 PERCENT	 [Date] = CONVERT(VARCHAR(10), [InspectionStartTime], 101)
			,[Inspections].[InspectionName] AS [InspectionString]
			,[DefectCount] = COUNT([Defects].[Id])
	FROM	 [dbo].[Inspections] 
			 INNER JOIN [dbo].[Defects]
					 ON [Defects].[InspectionID] = [Inspections].[Id] 
	GROUP BY CONVERT(VARCHAR(10), [InspectionStartTime], 101)
			,[Inspections].[InspectionName] 
	ORDER BY CONVERT(VARCHAR(10), [InspectionStartTime], 101)
			,[Inspections].[InspectionName];
