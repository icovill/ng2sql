﻿-- =============================================
-- Author:		Ian Covill
-- Create date: 11/1/2018
-- Description:	Creates a Bedload View Map in 
--	form of Geometric Line String for use in SQL Reports.
-- =============================================
CREATE PROCEDURE [report].[LiteCornerGeometryMap] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
DECLARE @LineString AS NVARCHAR(MAX)='POLYGON(('
DECLARE @GeomTable AS TABLE(Inspection NVARCHAR(MAX), LiteIndex INT NOT NULL, LineGeo Geometry)
DECLARE @X AS FLOAT
DECLARE @Y AS FLOAT
DECLARE @Corners CURSOR
DECLARE Inspections CURSOR FOR SELECT DISTINCT Inspection FROM report.CornersLiteRef
DECLARE @Inspection AS NVARCHAR(MAX)

DECLARE @i int = 0
DECLARE @ii int = 0
DECLARE @iii int=0

OPEN Inspections
WHILE @iii<(SELECT COUNT(DISTINCT Inspection) FROM report.CornersLiteRef)
BEGIN
	FETCH NEXT FROM Inspections INTO @Inspection
	WHILE @i < (SELECT COUNT(DISTINCT [Index]) FROM report.CornersLiteRef WHERE Inspection= @Inspection)
	BEGIN
		SET @Corners = CURSOR FOR (SELECT TrueCDPos, TrueMDPos FROM report.CornersLiteRef WHERE Inspection= @Inspection AND [Index] = @i)
		OPEN @Corners

		WHILE @ii < (SELECT COUNT(TrueCDPos) FROM report.CornersLiteRef WHERE Inspection = @Inspection AND [Index] = @i)
		BEGIN
			FETCH NEXT FROM @Corners INTO @X,@Y
			IF @ii <> 0
			BEGIN
				SET @LineString=CONCAT(@LineString, ',')
			END
			SET @LineString= CONCAT(@LineString, CAST(@X AS NVARCHAR(MAX)), ' ', CAST(@Y AS NVARCHAR(MAX)))
			SET @ii=@ii+1
		END
		SET @LineString=CONCAT(@LineString, '))')
		INSERT INTO @GeomTable VALUES (@Inspection, @i,geometry::STGeomFromText(@LineString,0))
		CLOSE @Corners
		SET @ii=0
		SET @LineString='POLYGON(('
		SET @i=@i+1
	END
	SET @iii=@iii+1
	SET @i=0
END
CLOSE Inspections
SELECT Inspection, LiteIndex, LineGeo FROM @GeomTable
END