﻿-- =============================================
-- Author:		Ian Covill
-- Create date: 11/1/2018
-- Description:	Creates a Bedload View Map in 
--	form of Geometric Line String for use in SQL Reports.
-- =============================================
CREATE PROCEDURE [report].[DefectPointGeometryMap] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
DECLARE @PointString AS NVARCHAR(MAX)='POINT('
DECLARE @GeomTable AS TABLE(Inspection NVARCHAR(MAX), LiteIndex INT NOT NULL, ClassName NVARCHAR(MAX), PointGeo Geometry)
DECLARE @X AS FLOAT
DECLARE @Y AS FLOAT
DECLARE @Classname AS NVARCHAR(MAX)
DECLARE @Defects CURSOR
DECLARE Inspections CURSOR FOR SELECT DISTINCT Inspection FROM report.DefectsLiteRef
DECLARE @Inspection AS NVARCHAR(MAX)

DECLARE @i int = 0
DECLARE @ii int = 0
DECLARE @iii int=0

OPEN Inspections
WHILE @iii<(SELECT COUNT(DISTINCT Inspection) FROM report.DefectsLiteRef)
BEGIN
	FETCH NEXT FROM Inspections INTO @Inspection
	WHILE @i < (SELECT COUNT(DISTINCT [Index]) FROM report.DefectsLiteRef WHERE Inspection= @Inspection)
	BEGIN
		SET @Defects = CURSOR FOR (SELECT TrueCDPos, TrueMDPos, ClassName FROM report.DefectsLiteRef WHERE Inspection= @Inspection AND [Index] = @i)
		OPEN @Defects

		WHILE @ii < (SELECT COUNT(TrueCDPos) FROM report.DefectsLiteRef WHERE Inspection = @Inspection AND [Index] = @i)
		BEGIN
			FETCH NEXT FROM @Defects INTO @X,@Y,@ClassName
			SET @PointString= CONCAT('POINT(', CAST(@X AS NVARCHAR(MAX)), ' ', CAST(@Y AS NVARCHAR(MAX)), ' )')
			SET @ii=@ii+1
			INSERT INTO @GeomTable VALUES (@Inspection, @i, @ClassName,geometry::STGeomFromText(@PointString,0))
		END
		CLOSE @Defects
		SET @ii=0
		SET @i=@i+1
	END
	SET @iii=@iii+1
	SET @i=0
END
CLOSE Inspections
SELECT Inspection, LiteIndex, ClassName, PointGeo FROM @GeomTable
END